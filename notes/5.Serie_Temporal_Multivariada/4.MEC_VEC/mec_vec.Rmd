---
title: <center> <h2> <b> Cointegração e Vetor de Correção de Erros (VEC) </b> </h2> </center> 
author: <center> Hudson Chaves Costa </center>
graphics: yes
linkcolor: blue
output: 
  html_notebook:
    theme: cerulean
    fig_caption: yes
references:
- id: tsay2014introduction
  title: An introduction to analysis of financial data with R
  author:
  - family: Tsay
    given: Ruey S
  publisher: John Wiley \& Sons
  type: book
  issued:
    year: 2014
- id: tsay2010analysis
  title: Analysis of financial time series
  author:
  - family: Tsay
    given: Ruey S
  publisher: John Wiley \& Sons
  type: book
  issued:
    year: 2010
- id: tsay2013multivariate
  title: Multivariate time series analysis with R and financial application
  author:
  - family: Tsay
    given: Ruey S
  publisher: John Wiley \& Sons
  type: book
  issued:
    year: 2013
- id: tiao1981modeling
  title: Modeling multiple time series with applications
  author:
  - family: Tiao
    given: George C
  - family: Box
    given: George EP
  publisher: Journal of the American Statistical Association
  type: article-journal
  volume: 76
  page: 802-816
  issued:
    year: 1981
- id: granger1969investigating
  title: Investigating causal relations by econometric models and cross-spectral methods
  author:
  - family: Granger
    given: Clive WJ
  publisher: Econometrica Journal of the Econometric Society
  type: article-journal
  page: 424-438
  issued:
    year: 1969
- id: sims1980macroeconomics
  title: Macroeconomics and reality
  author:
  - family: Sims
    given: Christopher A
  publisher: Econometrica Journal of the Econometric Society
  type: article-journal
  page: 1-48
  issued:
    year: 1980
- id: lutkepohl2005new
  title: New introduction to multiple time series analysis
  author:
  - family: Lutkepohl
    given: Helmut
  publisher: Springer Science & Business Media
  type: book
  issued:
    year: 2005
- id: phillips1986multiple
  title: Multiple time series regression with integrated processes
  author:
  - family: Phillips
    given: Peter CB
  - family: Durlauf
    given: Steven N
  publisher: The Review of Economic Studies
  type: article-journal
  volume: 53
  page: 473-495
  issued:
    year: 1986
nocite: | 
  @tsay2014introduction, @tsay2010analysis, @tsay2013multivariate, @tiao1981modeling, @granger1969investigating, @sims1980macroeconomics, @lutkepohl2005new, @phillips1986multiple
---

Este material tem como objetivo introduzir os conceitos sobre **Cointegração e Vetor de Correção de Erros (VEC)**. Vamos entender como avaliar se uma série temporal multivariada é cointegrada e como usar o modelo VEC.

##### **INTRODUÇÃO**

Um dos objetivos da Econometria é avaliar empiricamente teorias econômicas que, em geral, pressupõem relações de equilíbrio de longo prazo entre variáveis. Esta averiguação pode ser feita com base em modelagem de séries temporais que, via de regra, apresentam algum tipo de tendência. 

Neste sentido, fazer uso de modelos de regressão que envolvem dados de séries temporais apresentando tendência pode gerar resultados inconsistentes. Isso acontece porque as técnicas tradicionais de regressão, tal como **Mínimos Quadrados Ordinários (MQO)**, precisam que as variáveis sejam estacionárias, ou seja, que a média condicional e a variância condicional não oscilem com o tempo. 

Como alternativa, surge o **Modelo de Correção de Erros (MCE)** que utiliza a análise de cointegração para a estimação de modelos quando as variáveis não apresentam estacionariedade. A idéia intuitiva de **cointegração** é que variáveis não estacionárias podem caminhar juntas, isto é, podem ter trajetórias temporais interligadas, de forma que no longo prazo apresentem relação de equilíbrio. 

Para o caso multivariado, estudamos como o **Modelo Vetorial Autorregressivo (VAR)**, opcionalmente estimado por MQO, pode ser usado para modelagem de séries temporais multivariadas. Porém, temos a possibilidade dos componentes da série temporal multivariada serem cointegrados e neste caso o VAR não deveria ser usado e sim o **Modelo Vetorial de Correção de Erros (VEC)** que é a versão vetorial do MCE. 

Resumidamente, no decorrer deste documento entenderemos o que é **cointegração**, sua relação com **estacionariedade** e como estes conceitos conduzirão a análise de uma série temporal multivariada da seguinte maneira:

* Se **todos os componentes da série são estacionários**, o modelo VAR em nível se aplica sem problemas;
* Se **temos componentes não estacionários**, contamos com duas alternativas:
    * Se eles **são não estacionários e não cointegrados**, deve-se ajustar o VAR em primeiras diferenças
    * Se eles **são não estacionários, mas cointegrados**, deve-se ajustar o Vetor de Correção de Erros (VEC)


##### **COINTEGRAÇÃO E REGRESSÃO ESPÚRIA**

Como sabemos, muitas séries temporais econômicas são **estacionárias em primeira diferença**. Processos estacionários em primeira diferença são também conhecidos como **processos integrados de ordem 1** ou processos $I(1)$. Em geral, um processo cuja $d$-ésima diferença é estacionária é um processo integrado de ordem $d$, ou $I(d)$.

Um exemplo clássico de processo estacionário em primeira diferença é o **passeio aleatório**. Ele é uma variável qualquer (aqui, $p_t$) que pode ser escrita como:

$$
p_t=p_{t-1}+a_{t}
$$
onde $a_t$ é independentemente e identicamente distribuído (iid) com média zero e variância constante. Embora $E\left[p_t\right]=0$[^1] para todo $t$, sua variância $Var(p_t)=T\sigma^{2}$ não é invariante no tempo. Assim, $p_t$ não é estacionário e em função de $\Delta p_t=p_t - p_{t-1} = a_t$ e $a_t$, por definição, ser estacionário, a primeira diferença de $p_t$ será estacionária.

[^1]: Supondo $p_0=0$, temos que $p_1=a_1$, $p_2=p_1+a_2=a_1+a_2$ e $p_t=a_1+a_2+...+a_t$ e assim, $E\left[p_t \right]=E\left[a_1 \right]+...+E\left[a_t \right]=0$ e $Var(p_t)=Var(a_1)+...+Var(a_t)=t\sigma^{2}_{a}$

Agora, suponha que queremos estimar o seguinte modelo de regressão linear simples:

$$
y_t = \alpha + \beta x_t + a_t
$$
onde $E[a_t]=0$, $Var(a_t)=\sigma^{2}$ e $E\left[a_t, a_{t-l}\right]=0$ para todo $l >0$. Se $y_t$ e $x_t$ são originadas por passeios aleatórios independentes (que por definição não são estacionários), não existe relação entre $y_t$ e $x_t$ e os parâmetros estimados por MQO para o modelo de regressão linear simples produzirá uma estimativa para $\beta$ inconsitente. Este caso é conhecido na literatura como **regressão espúria**.

Uma alternativa para o **problema de regressão espúria** seria estimar o seguinte modelo:

$$
\begin{aligned}
&&& y_t - y_{t-1} = \alpha - \alpha + \beta x_t - \beta x_{t-1} + a_t - a_{t-1}\\
\\ 
&&& \Delta y_t = \beta \Delta x_t + \varepsilon_t \\
\end{aligned}
$$
dado que $\Delta y_t$ e $\Delta x_t$ serão estacionários. Porém, isso pode esconder as propriedades de longo prazo da relação entre as duas variáveis. 

```{r, echo=FALSE, message=FALSE, warning=FALSE, fig.width=9, fig.height=5}
##########################
####     PACOTES     #####
##########################

# Carregar no ambiente os pacotes necessários para replicar os códigos abaixo
suppressMessages(require(quantmod))
suppressMessages(require(Quandl))
suppressMessages(require(forecast))
suppressMessages(require(dplyr))
suppressMessages(require(magrittr))
suppressMessages(require(highcharter))
suppressMessages(require(dygraphs))
suppressMessages(require(ggplot2))
suppressMessages(require(MTS))
suppressMessages(require(vars))
suppressMessages(require(urca))
suppressMessages(require(fUnitRoots))
suppressMessages(require(stargazer))

##########################
####       DADOS     #####
##########################

# Semente para fazer o exemplo produzir os mesmos resultados 
# em computadores diferentes
set.seed(123)

# Repetir zero 1000 vezes para armazenar dados simulados de
# y e x
y <- rep(0, 1000)
x <- rep(0, 1000)

# Criar o passeio aleatório (Yt = Yt-1 + ERROt) onde o erro 
# é um ruído branco, conforme estudado em sala. Lembre-se
# que na definição em sala temos que supor a existência de
# um valor inicial para o passeio aleatório e a partir deste
# valor inicia-se a geração dos dados (aqui, assumimos que
# este valor é 0, observe que a iteração do for inicia em 2).
# A função rnorm gerará aleatóriamente um valor para uma Normal
# com média 0 e variância 1.
for (i in 2:1000) {
  y[i] <- y[i-1] + rnorm(1)
}

# Criar o passeio aleatório (Xt = Xt-1 +ERROt), assim como 
# fizemos para y
for (i in 2:1000) {
  x[i] <- x[i-1] + rnorm(1)
}

##########################
####    GRÁFICOS     #####
##########################

# Salvar os dados em uma série temporal multivada usando
# a função cbind (juntar séries em colunas)
passeios <- cbind(y = as.ts(y), x = as.ts(x))

# Gráfico das duas séries temporais usando o pacote dygraphs
dygraphs::dygraph(passeios, main = "Passeios Aleatórios Simulados")
```

Pelo gráfico podemos observar que há uma aparecente relação entre as duas variáveis. Sem fazer qualquer tipo de teste para avaliar a estacionariedade delas, uma pessoa poderia estimar um modelo de regressão linear simples e fazer inferência com os resultados ou até mesmo previsões para $y$. Como já sabemos que ambas as variáveis foram geradas por um passeio aleatório e que este, por definição, não é estacionário, entendemos que os resultados não têm qualquer utilidade. Abaixo, resultado desta regressão.

```{r, echo=FALSE, message=FALSE, warning=FALSE}
##########################
####    REGRESSÃO    #####
##########################

# Estimação da Regressão Linear Simples via OLS. Aqui, usamos
# a função lm do pacote stats que tem as seguintes opções:
# - formula: modelo a ser ajustato (~ faz o papel de "=")
# - data: o cojunto de dados
# - weights: os pesos para regressão ponderada
# - subset: sub-conjunto dos dados
# - na.action: especificar o que fazer no cado de NA nos dados. Como
# padrão usa a função na.omit que exclui da base casos de NA
modelo <- lm(formula = y~x, data = passeios)

##########################
####   RESULTADOS    #####
##########################

# Mostrar modelo estimado
stargazer(modelo, type = "text", title = "Resultado Regressão Espúria")
```

Como esperado, o resultado mostra que os parâmetros estimados são altamente significativos e há autocorrelação nos resíduos como apresenta o gráfico da Função de Autocorrelação (FAC) abaixo. Este resultado indica **regressão espúria**, caracterizada por relação forte entre as variáveis, devido a tendência estocástica comum às duas séries (fruto do passeio aleatório neste caso) e erro não estacionário (gráfico dos resíduos do modelo estimado).

```{r, echo=FALSE, message=FALSE, warning=FALSE, fig.width=12, fig.height=5}
##########################
### FAC DOS RESÍDUOS  ####
##########################

# Calcular a FAC dos resíduos do modelo
acf_residuos <- acf(modelo$residuals, plot = FALSE, na.action = na.pass, max.lag = 25)

##########################
####   RESULTADOS    #####
##########################

par(mfrow = c(1, 2))
# Gráfico da FAC
plot(acf_residuos, main = "", ylab = "", xlab = "Defasagem")
title("Função de Autocorrelação (FAC) dos Resíduos", adj = 0.5, line = 1)

# Gráfico dos Resíduos
plot(modelo$residuals, main = "Resíduos de MQO", type = "l", ylab = "", xlab = "")
```

##### **MODELO DE CORREÇÃO DE ERROS**

@phillips1986multiple demonstraram que é possível trabalhar com o nível das séries sem correr o risco de regressões espúrias desde que as séries utilizadas sejam cointegradas de uma particular ordem. 

Para o nosso exemplo, quando $\hat{a}_t = y_t - \hat{\alpha} - \hat{\beta} x_t$ é $I(0)$, dizemos que $y_t$ e $x_t$ são cointegradas, pois cada variável é $I(1)$ mas a combinação linear delas, $\hat{a}_t$, é $I(0)$. Como consequência, assumimos que $y_t$ e $x_t$ têm trajetórias temporais interligadas de forma que no longo prazo apresentem relação de equilíbrio. Entretanto, no curto prazo há desvios dessa relação de equilíbrio de modo que $a_t$ é chamado de **erro de equilíbrio**, porque expressa os **desvios temporais de equilibrio de longo prazo**. 

O **Modelo de Correção de Erros (MCE)** corrige esses desequilíbrios e nos mostra a taxa à qual o sistema retorna ao equilíbrio após os desvios. Para entender como ele faz isso suponha que duas variáveis $y$ e $x$ são $I(1)$, mas cointegradas e que queremos estimar o seguinte modelo: 

$$
y_t = \beta_0 + \beta_{1}y_{t-1} + \beta_{2}x_t + \beta_{3}x_{t-1} + \varepsilon_{t}
$$
Tal modelo é conhecido como **Modelo Autorregressivo com Defasagens Distribuídas** (do inglês, ADL - *Autoregressive Distributed Lag*). Repare que temos uma combinação entre valores defasados das duas variáveis, além da própria variável $x_t$[^2], e que este modelo é semelhante às equações estimadas no VAR.

[^2]: Quando a dinâmica do modelo é ditada pelo comportamento das variáveis independentes defasadas nos referimos a **modelos com defasagens distribuídas**, sendo as defasagens responsáveis por explicar $y_t$. Se somente os valores passados de $y_t$ determinam seu valor em $t$, a dinâmica de $y_t$ pode ser descrita segundo **Modelos Autorregressivos** (AR). É possível, ainda, combinar os dois modelos anteriores em uma única equação, originando os chamados **Modelos Autorregressivos com Defasagens Distribuídas** (do inglês, ADL - *Autoregressive Distributed Lag*).

Podemos reescrever o modelo como: 

$$
\begin{aligned}
& y_t - y_{t-1} = \beta_0 + \beta_1y_{t-1} - y_{t-1} + \beta_2x_t + \beta_3x_{t-1} + \varepsilon_t \\
& \Delta y_t = \beta_0 - (1-\beta_1)y_{t-1} + \beta_2x_t + \beta_3x_{t-1} + \varepsilon_t \\
& \Delta y_t = \beta_0 - (1-\beta_1)y_{t-1} + \beta_2x_t + \beta_3x_{t-1} + \beta_2x_{t-1} - \beta_2x_{t-1}  + \varepsilon_t \\
& \Delta y_t = \beta_0 - (1-\beta_1)y_{t-1} + \beta_2 \Delta x_t + \beta_3x_{t-1} + \beta_2x_{t-1} + \varepsilon_t \\
& \Delta y_t = \beta_0 - (1-\beta_1)y_{t-1} + \beta_2 \Delta x_t + (\beta_3 + \beta_2)x_{t-1} + \varepsilon_t \\
& \Delta y_t = \gamma \Delta x_t -\lambda(y_{t-1} -\alpha - \beta x_{t-1}) + \varepsilon_t \\
\end{aligned}
$$
onde $\gamma = \beta_2$, $\lambda = 1 -\beta_1$, $\alpha=\frac{\beta_0}{1-\beta_1}$, $\beta = \frac{\beta_3+\beta_2}{1-\beta_1}$ e $y_{t-1} -\alpha - \beta x_{t-1} = a_{t-1}$ que é estimado por $y_{t} = \alpha + \beta x_{t} + a_t$. Observe que o **coeficiente do erro de correção é negativo** por construção significando que a correção do erro é feita em cada período. Para facilitar o entendimento, suponha que $y_t = venda_t$ e que $x_t=preco_t$. Assim, o Modelo de Correção de Erros (MCE) se torna:

$$
\begin{aligned}
& \Delta venda_t = \gamma \Delta preco_t -\lambda(venda_{t-1} -\alpha - \beta preco_{t-1}) + \varepsilon_t \\
& \Delta venda_t = \gamma \Delta preco_t -\lambda(a_{t-1}) + \varepsilon_t \\
\end{aligned}
$$
que deixa claro que a variação nas vendas é explicada por dois componentes: **um termo de curto prazo e outro de longo prazo**. As variações nos preços, $\Delta preco_t$, representam o termo de curto prazo e $a_{t-1}$ é o componente de longo prazo, pois pode ser entendido como o termo de erro da regressão $venda_{t} = \alpha + \beta preco_{t} + a_t$ que nada mais é que uma combinação linear estacionária entre as duas variáveis.

* **ESTIMAÇÃO DO MODELO MCE**

A estimação do Modelo de Correção de Erros pode ser feita da seguinte maneira: 

i) Estimar a relação entre $y_t$ e $x_t$ por meio de $y_t = \alpha + \beta x_{t} + a_t$.
ii) Obter $\hat{a}_t$ que são os resíduos de cointegração.
iii) Estimar $\Delta y_t = \gamma \Delta x_t +\lambda\hat{a}_{t-1}+ \varepsilon_t$ e obter a estimativa para os termos de curto ($\gamma$) e longo prazo ($\lambda$).

##### **VETORES DE COINTEGRAÇÃO**

O vetor de cointegração é formado pelos coeficientes da relação de cointegração (estacionária) que assegura o equilíbrio de longo prazo entre as séries. Suponto $\boldsymbol{r}_{t}$ uma série temporal multivariada, os $k$ elementos do vetor $\boldsymbol{r}_{t} = \left(r_{1t},...,r_{kt}\right)^{'}$ são ditos cointegrados de ordem $(d,b)$ se:

i. Todos os elementos de $\boldsymbol{r}_{t}$ são integrados de ordem $d$, ou seja, são $I(d)$;
ii. Existe um vetor não nulo $\boldsymbol{\beta}$, tal que $\boldsymbol{a}_{t} = \boldsymbol{\beta}^{'}\boldsymbol{r}_{t}$ é $I(d-b)$, $b>0$. A diferença $d-b$ é a ordem de integração obtida da aplicação do vetor $\boldsymbol{\beta}$ em $\boldsymbol{r}_{t}$. Se as variáveis são cointegradas, o resíduo $\boldsymbol{a}_{t}$ tem ordem de integração menor que a ordem das variáveis que o originaram.

O número de vetores de cointegração depende do número de variáveis envolvidas. Tem-se:

a. Caso de duas variáveis: Se $\boldsymbol{r}_{t} = \left(y_t, r_t\right)^{'}$ com $y_t \sim I(1)$, $x_t \sim I(1)$ e $a_t = y_t - \beta x_t \sim I(0)$, então dizemos que $y_t$ e $x_t$ $\sim CI(1,1)$, ou seja, são cointegradas na ordem $(1,1)$ com vetor de cointegração $\boldsymbol{\beta} = \left(1,-\hat{\beta}\right)^{'}$ e o sistema é cointegrado dado que $\boldsymbol{\beta}^{'} \boldsymbol{r}_{t} \sim I(0)$. Neste caso, existe somente **uma** combinação linear estacionária que representa **uma** relação de equilíbrio de longo prazo entre as variáveis que é representada por:

$$
\boldsymbol{a}_{t} = \boldsymbol{\beta}^{'} \boldsymbol{r}_{t} = \begin{bmatrix} 1 & -\hat{\beta} \end{bmatrix} \begin{bmatrix} y_t \\ x_t  \end{bmatrix} = y_t -\hat{\beta} x_t
$$

b. Caso de $k$ variáveis: Se $\boldsymbol{r}_{t} = \left(y_{1t}, x_{1t}, x_{2t}, ..., x_{kt}\right)^{'}$ com $y_t \sim I(1)$, $x_{1t} \sim I(1)$, $x_{2t} \sim I(1)$, $x_{3t} \sim I(1)$, ..., $x_{kt} \sim I(1)$ e $a_t = y_t - \beta_1x_{1t} - \beta_2x_{2t}, ..., - \beta_kx_{kt} \sim I(0)$, então dizemos que $y_t$, $x_{1t}$, ..., $x_{kt}$ $\sim CI(1,1)$, ou seja, são cointegradas na ordem $(1,1)$ com vetor de cointegração $\boldsymbol{\beta} = \left[1, -\hat{\beta}_1, -\hat{\beta}_2,...,-\hat{\beta}_k\right]^{'}$ e o sistema é cointegrado dado que $\boldsymbol{\beta}^{'} \boldsymbol{r}_{t} \sim I(0)$. Neste caso, pode existir até $k-1$ vetores de cointegração linearmente independentes. Ou seja, podem existir de $1$ até $k-1$ vetores de cointegração que representam relações de equilíbrio de longo prazo entre as variáveis.

* **RANK DE COINTEGRAÇÃO**

O rank de cointegração ($r$) é o número de vetores de cointegração linearmente independentes. Para $k$ variáveis de mesma ordem de integração e cointegradas, tem-se que $1\leq r \leq k-1$. O rank de cointegração é o número de relações de cointegração importantes para manter o equilíbrio de longo prazo entre as variáveis.

##### **TESTES DE COINTEGRAÇÃO**

Para testar a existência de cointegração entre variáveis, podemos usar **testes de uma equação** que se baseiam no ajustamento da relação entre as variáveis e **testes com várias equações** onde ajustamos um modelo VAR com as variáveis a serem testadas. Em ambos os casos podemos considerar 2 ou mais variáveis. 

* **TESTE DE UMA EQUAÇÃO**

Para este caso, o teste comumente usado é o de **Engle-Granger** que consiste em ajustar uma relação entre as variáveis e realizar o teste de raiz unitária de Dickey-Fuller Aumentado (ADF) nos resíduos da equação ajustada. Para o caso de 2 variáveis ($y$ e $x$, por exemplo) temos o seguinte processo:

1. Executar o teste de raiz unitária para $y$ e $x$ e certificar que elas são $I(1)$. Se elas forem $I(0)$ não há razão para testar cointegração.
2. Estimar a relação $y_t = \alpha + \beta x_t + a_t$ e obter $\hat{a}_t$
3. Testar se os resíduos são estacionários, ou seja, se são $I(0)$ usando o teste de Dickey-Fuller Aumentado[^3] (a equação do teste não deve ter intercepto nem tendência porque os resíduos de MQO oscilam em torno de zero), conforme abaixo:

$$
\begin{aligned}
& \Delta \hat{a}_t = \phi\hat{a}_{t-1}+\sum_{i=1}^{p-1}{\lambda_{i}\Delta \hat{a}_{t-i}}+\xi_t \\
& \\
& H_0: \phi=0~\rightarrow \hat{a}_t~\text{não estacionário}~\rightarrow y~\text{e}~x~\text{não são cointegradas} \\
& H_1: \phi<0~\rightarrow \hat{a}_t~\text{estacionário}~\rightarrow y~\text{e}~x~\text{são cointegradas} \\
\end{aligned}
$$

[^3]: Aqui, como explicado no texto, vericamos se a série se comporta como um passeio aleatório sem drift e tendência, ou seja, $p_t= p_{t-1} + a_t$. Relembrando, a equação do teste DF é $p_t=\phi p_{t-1} + a_t$ e testamos se $H_0:\phi = 1$ (não estacionária) ou $H_1: \left|\phi\right|<1$ (estacionária). Outra forma de especificar a equação do teste é fazer $p_t-p_{t-1}=\phi p_{t-1} - p_{t-1} + a_t \rightarrow  \Delta p_t = (\phi -1)p_{t-1}+a_t$ o que permite reescrever o teste como $\Delta p_t = \pi p_{t-1} + a_t$ e testar se $\pi = 0$ (não estacionária, pois implica em $\phi=1$) e $\pi<0$ (estacionária, pois implica em $\phi<1$). Observe que esta última formulação é a usada no nosso teste, sendo que a diferença é que usamos mais defasagens da variável a ser testada na equação do teste (por isso, chamamos de DF aumentado).

* **TESTE COM VÁRIAS EQUAÇÕES**

Suponha $k$ variáveis $I(1)$ e que a teoria ou qualquer conhecimento *a priori*, sugere uma relação de equilíbrio de longo prazo entre elas. Em geral, existem $r\leq k-1$ combinações lineares independentes $I(0)$ que são chamadas relações de cointegração e o problema é determinar o valor de $r$. O teste mais usado para este fim é o **teste de Johansen** que tem como base o modelo VAR. Considere um $VAR(p)$ com $k$ variáveis

$$
Y_t = A_1Y_{t-1}+A_2Y_{t-2}+...+A_pY_{t-p}+\epsilon_t
$$
À semelhança do teste de Dickey-Fuller Aumentado (ADF), o teste de Johansen se baseia em um modelo transformado, denominado de VAR reparametrizado, que permite um processo autorregressivo de ordem $p$ e não somente de ordem $1$. A obtenção deste modelo segue procedimento semelhante à derivação da equação de teste do Dickey-Fuller Aumentado (ADF)[^4]. Partindo-se do VAR anterior obtém-se o VAR reparametrizado representado por: 

[^4]: Para melhor entendimento, vamos considerar a derivação a partir do modelo VAR(1) com $k$ variáveis, $Y_{t} = A_1Y_{t-1}+\epsilon_t$. O VAR reparametrizado é dado por $\Delta Y_t = \Pi Y_{t-1}+\epsilon_t$ em qye $\Pi = -(I - A_1)$. Esta forma é obtida somando e subtraindo do lado direito da equação o vetor de variáveis defasadas $Y_{t-1}$, isto é, $Y_{t-1}= A_1Y_{t-1} + \epsilon_t + Y_{t-1} - Y_{t-1} \Rightarrow  \Delta Y_t = (A_1 - I)Y_{t-1} + \epsilon_t \Rightarrow \Delta Y_t = -(I-A_1)Y_{t-1} + \epsilon_t \Rightarrow \Delta Y_t = \Pi Y_{t-1} + \epsilon_t$ em que $\Pi = -(I-A_1)$ que é o VAR reparametrizado do VAR(1).

$$
\begin{aligned}
& \Delta Y_t = \Pi Y_{t-1} + \sum_{i=1}^{p-1}{\Gamma_i\Delta Y_{t-i}} +\epsilon_t \\
\end{aligned}
$$
em que $\Gamma_i = -\sum_{j=i+1}^{p}{A_j}$ e $\Pi=\sum_{i=1}^{p}{A_i-I}=\left(I_k -\sum_{i=1}^{p}{A_i}\right)$. Perceba a semelhança entre o teste de Johansen e o teste de raiz unitária Dickey-Fuller Aumentado (ADF). Inicialmente, o termo $\Pi Y_{t-1}$ representa $k$ combinações lineares das variáveis, isto é:

$$
\begin{aligned}
\Pi Y_{t-1} & = \begin{bmatrix}
\pi_{11} & \pi_{12} & ... & \pi_{1k} \\
\pi_{21} & \pi_{22} & ... & \pi_{2k} \\
\vdots   & \vdots   &     & \vdots \\
\pi_{k1} & \pi_{k2} & ... & \pi_{kk} \\
\end{bmatrix} 
\begin{bmatrix}  
Y_{1,t-1} \\
Y_{2,t-1} \\
\vdots   \\
Y_{k,t-1} \\ \end{bmatrix} \\
&&& \\
& = \left\{\begin{matrix}
\pi_{11}Y_{1,t-1}+\pi_{12}Y_{2,t-1}+ ... + \pi_{1k}Y_{k,t-1} \\ 
... \\ 
... \\ 
\pi_{k1}Y_{1,t-1}+\pi_{k2}Y_{2,t-1}+ ... + \pi_{kk}Y_{k,t-1}
\end{matrix}\right.
\end{aligned}
$$

que são $k$ combinações lineares. Por definição, todos os termos da equação são estacionários, exceto $\Pi Y_{t-1}$. Para o sistema ser estacionário, $\Pi Y_{t-1}$ deve ser estacionário e para isso a matriz $\Pi$ deve apresentar estrural tal que as combinações lineares sejam estacionárias. Para as variáveis serem cointegradas as linhas de $\Pi$ não podem ser todas linearmente independentes. Assim, $\Pi$ deve ser singular, ou seja $det(\Pi)=0$ e, então, o posto ou rank de $\Pi$ deve ser menor que $k$ para que as variáveis sejam cointegradas. 

Resumindo, temos $3$ possibilidades para o teste de Johansen:

1. $Posto(\Pi)=0$[^5]
    * Significa que $\Pi=0$ que é a analogia ao caso onde $\phi=0$ no teste ADF e assim, temos $k$ raízes unitárias
    * Não há relação de cointegração entre as variáveis e não existe mecanismo de correção de erro
    * **Decisão**: O modelo VAR deve ser especificado em primeiras diferenças
2. $Posto(\Pi)=k$
    * As linhas de $\Pi$ são linearmnete independentes e o $\left|\Pi \right| \neq 0$ 
    * Existem $k$ combinações estacionárias das variáveis, mas cointegração não é pertinente dado que as variáveis são estacionárias
    * **Decisão**: O modelo VAR deve ser estimado em nível
3. $0\leq Posto(\Pi)=r \leq k$
    * Existem $r$ combinações lineares estacionárias e o $\left| \Pi \right| = 0$ com pelo menos uma linha ou coluna nula
    * Se as variáveis são $I(1)$, existem $r$ relações de cointegração que fornecem $r$ vetores de cointegração e o termo $\Pi Y_{t-1}$ fornecem as combinações lineares estacionárias
    * **Decisão**: O modelo VEC deve ser utilizado

[^5]: Seja uma matriz A de ordem $m \times n$. Define-se como posto da matriz A, $Posto(A)$, como sendo a mais alta ordem de determinante diferente de zero que pode ser calculado a partir das submatrizes de A. Através do posto da matiz podemos identificar se uma matriz quadrada é singular (determinante igual a zero) ou não singular (determinante diferente de zero), isto é, se A é uma matriz quadrada de ordem $k$, então: i) A é singular, se e somente se, $Posto(A)<k$, ii) A não é singular, se e somente se, $Posto(A)=k$. Se uma matriz tem posto nulo, ou seja, $Posto(A)=0$ dizemos que A é uma matriz nula (de zeros).

* **TESTE DO TRAÇO E TESTE DE RAIZ CARACTERÍSTICA MÁXIMA**

O procedimento de Johansen consiste em testar o número de raízes características diferentes de zero na matriz $\Pi$ que corresponde ao número de relações e vetores de cointegração entre as variáveis. São utilizados dois testes: Teste do Traço e Teste da Raiz Característica Máxima. 

1. Teste do Traço

Este teste considera como hipótese nula a existência de $r_0$ raízes características diferentes de zero ($r_0$ vetores de cointegração) contra a alternativa de $r>r_0$. Formalmente,

$$
\begin{aligned}
& H_0: r=r_0 \\
& H_1: r>r_0 \\
\end{aligned}
$$
A estatística de teste é dada por:

$$
\lambda_{traço} = -T \sum_{i=r_0+1}^{k}{\ln(1-\hat{\lambda}_i)}
$$
em que T é o número de observações e $\hat{\lambda}_i$ são as raízes características obtidas da matriz $\Pi$ estimada.

2. Teste da Raiz Característica Máxima

O segundo teste tem como hipótese nula a existência de $r_0$ raízes características diferentes de zero ($r_0$ vetores de cointegração) contra a alternativa de $r=r_0 +1$. Formalmente, 

$$
\begin{aligned}
& H_0: r=r_0 \\
& H_1: r=r_0 +1\\
\end{aligned}
$$
e a estatística de teste é:

$$
\lambda_{max} = -T \ln(1-\hat{\lambda}_{r_0+1})
$$
Os testes são realizados em sequência, de forma crescente, até que a hipótese nula não seja rejeitada. Para $H_0: r=0$, rejeitar $H_0$ significa que há um ou mais vetores de cointegração, pelo teste do traço, e um pelo teste da raiz máxima. Para $H_0: r=1$, rejeitar $H_0$ significa que há dois ou mais vetores de cointegração, pelo teste do traço, e mais um pelo teste da raiz máxima. 

##### **MODELO VETORIAL DE CORREÇÃO DE ERROS (VEC)**

Se o posto de $\Pi=r<k$, pode-se mostrar que existem matrizes $\alpha_{k\times r}$ e $\beta_{k\times r}$ tais que $\Pi_{k\times k}=\alpha_{k\times r}\beta^{'}_{r\times k}$ tais que:

$$
\Pi_{k\times k} = \alpha_{k \times r} \beta^{'}_{r \times k}
$$
Substituindo na equação do VAR reparametrizado, obtém-se:

$$
\begin{aligned}
& \Delta Y_t =  \alpha \beta^{'} Y_{t-1} + \sum_{i=1}^{p-1}{\Gamma_i\Delta Y_{t-i}} +\epsilon_t \\
\end{aligned}
$$
que é o modelo de correção de erro na forma multivariada denominado Modelo de Correção de Erro Vetorial (VEC). O VEC é um VAR (reparametrizado) com as restrições de cointegração entre as variáveis. Tem-se que:

* $\beta^{'}Y_{t-1}$: são as $r$ relações de cointegração que definem a trajetória de longo prazo (equilíbrio) entre as variáveis. 
* $\alpha$: matriz de coeficientes de ajustamento para o equilíbrio de longo prazo
* $\Gamma_i$: matrizes de coeficientes que definem a dinâmica de curto prazo

Como ilustração, considere um exemplo com $k=3$ variáveis e matriz $\Pi$ dada por:

$$
\Pi = \begin{bmatrix}
-\frac{1}{2} & -\frac{5}{16}  & -\frac{1}{16} \\ 
\frac{1}{8} & -\frac{41}{64}  & \frac{5}{32} \\ 
 \frac{1}{4} & -\frac{11}{32} & -\frac{3}{32}
\end{bmatrix}
$$

com raízes características $\lambda_{1}=0$, $\lambda_{2}=-0,4416$ e $\lambda_{3}=-0,7928$. Assim, com duas raízes características diferentes de zero (posto de $\Pi=2$) e existem 2 relações de cointegração. Pode-se mostrar que

$$
\Pi = \begin{bmatrix}
-\frac{1}{2} & -\frac{5}{16}  & -\frac{1}{16} \\ 
\frac{1}{8} & -\frac{41}{64}  & \frac{5}{32} \\ 
 \frac{1}{4} & -\frac{11}{32} & -\frac{3}{32}
\end{bmatrix} = \begin{bmatrix}
-\frac{1}{2} & \frac{1}{4} \\ 
\frac{1}{8} & -\frac{5}{8} \\ 
 \frac{1}{4} & \frac{3}{8} 
\end{bmatrix} \begin{bmatrix}
1 & -\frac{1}{8}  & 0 \\ 
0 & 1  & -\frac{1}{4}
\end{bmatrix}
$$
O modelo VEC, desconsiderando os termos de diferença defasados (${\Gamma_i\Delta Y_{t-i}}$), será:

$$
\begin{aligned}
\begin{bmatrix}
\Delta Y_{1t} \\ 
\Delta Y_{2t} \\ 
\Delta Y_{3t}
\end{bmatrix} &&& = \begin{bmatrix}
-\frac{1}{2} & \frac{1}{4} \\ 
\frac{1}{8} & -\frac{5}{8} \\ 
 \frac{1}{4} & \frac{3}{8} 
\end{bmatrix} \begin{bmatrix}
1 & -\frac{1}{8}  & 0 \\ 
0 & 1  & -\frac{1}{4}
\end{bmatrix} \begin{bmatrix}
Y_{1t-1} \\ 
Y_{2t-1} \\ 
Y_{3t-1}
\end{bmatrix} \\
&& \\
&&& = \begin{bmatrix}
-\frac{1}{2} & \frac{1}{4} \\ 
\frac{1}{8} & -\frac{5}{8} \\ 
 \frac{1}{4} & \frac{3}{8} 
\end{bmatrix}\begin{bmatrix}
Y_{1t-1}-\frac{1}{8}Y_{2t-1}+0Y_{3t-1}\\ 
0Y_{1t-1}+Y_{2t-1}-\frac{1}{4}Y_{3t-1}
\end{bmatrix} \\
&&& \\
&&& = \left\{\begin{matrix}
\Delta Y_{1t} = -\frac{1}{2} \left(Y_{1t-1}-\frac{1}{8}Y_{2t-1}\right) +\frac{1}{4} \left(Y_{2t-1}-\frac{1}{4}Y_{3t-1}\right) \\ 
\Delta Y_{2t} = \frac{1}{8} \left(Y_{1t-1}-\frac{1}{8}Y_{2t-1}\right) -\frac{5}{8} \left(Y_{2t-1}-\frac{1}{4}Y_{3t-1}\right) \\ 
\Delta Y_{3t} = \frac{1}{4} \left(Y_{1t-1}-\frac{1}{8}Y_{2t-1}\right) +\frac{3}{8} \left(Y_{2t-1}-\frac{1}{4}Y_{3t-1}\right)
\end{matrix}\right. \\
\end{aligned}
$$

As expressões $Y_{1t-1}-\frac{1}{8}Y_{2t-1}$ e $Y_{2t-1}-\frac{1}{4}Y_{3t-1}$ são as relações de cointegração que entram em cada equação. A maneira mais simples de estimar um modelo VEC é o procedimento em dois estágios. Primeiro, estimamos a relação de cointegração e criamos a série de defasagens dos resíduos. Após isso, estimamos a equação do modelo por meio de Mínimos Quadrados Ordinários (MQO).

##### **PROCESSO DE ESTIMAÇÃO**

Abaixo, os passos para estimação e avaliação dos modelos VAR, SVAR e VEC. Perceba que dependendo das decisões nas etapas, seguimos com a estimação do modelo VEC. 

1. Visualizar os dados e identificar observações fora do padrão (outliers, sazonalidade, tendência)
2. Se necessário, transformar os dados para estabilizar a variância (logaritmo ou retirar sazonalidade, por exemplo)
3. Avaliar a função de correlação cruzada para confirmar a possibilidade de modelagem multivariada.
4. Testar se os dados são estacionários ou cointegrados:
    * Caso não tenha raiz unitária (estacionários), estimar VAR com as séries em nível
    * Caso tenha raiz unitária, mas sem cointegração é preciso diferenciar os dados até se tornarem estacionários e estimar VAR com as séries diferenciadas
    * Caso tenha raiz unitária, mas com cointegração devemos estimar o VEC com as séries em nível
5. Definir a ordem $p$ para os dados em análise por meio de critérios de informação (escolher modelo com menor AIC, por exemplo)
6. Estimar o modelo escolhido no passo 4
    * Se VAR (forma reduzida):
        - Verificar significância estatística do modelo estimado e, caso seja necessário, eliminar parâmetros não significantes.
        - Analisar a causalidade de Granger (variáveis que não granger causa as demais podem ser retiradas do modelo)
    * Se SVAR (forma estrutural):
        - Definir a estrutura para as matrizes A e B e o modelo de interesse (A, B ou AB)
        - Verificar significância estatística do modelo estimado e, caso seja necessário, eliminar parâmetros não significantes.
        - Analisar a causalidade de Granger (variáveis que não granger causa as demais podem ser retiradas do modelo)
    * Se VEC (Modelo Vetorial de Correção de Erros)
        - Usar a quantidade de vetores de cointegração obtidos no teste de cointegração para estimar o modelo VEC
8. Examinar se os resíduos se comportam como ruído branco e condições de estacionariedade do modelo. Caso contrário, retornar ao passo 3 ou 4.
    * Verificar a autocorrelação serial por meio da FAC e FACP dos resíduos de cada equação do modelo estimado. O ideal é não ter defasagens significativas.
    * Verificar correlação cruzada por meio da FCC dos resíduos.
    * Analisar a estabildiade do modelo estimado através dos autovalores associados ao mesmo.
    * Verificar a distribuição de probabilidade (Normal) para os resíduos de cada equação do modelo.
    * Analisar heterocedasticidade condicional (resíduos devem ser homocedasticos, ou seja, variância condicional constante)
9. Uma vez que os resíduos são ruído branco e o modelo é estável:
    * Analisar funções de resposta ao impulso
    * Analisar a importância das variáveis para explicar a variância do erro de previsão de cada variável
    * Fazer previsões paras as variáveis do modelo

##### **REFERÊNCIAS**

