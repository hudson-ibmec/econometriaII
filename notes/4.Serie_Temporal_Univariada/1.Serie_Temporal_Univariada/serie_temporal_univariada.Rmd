---
title: <center> <h2> <b>Séries Temporais Univariadas </b> </h2> </center> 
author: <center> Hudson Chaves Costa </center>
graphics: yes
linkcolor: blue
output: 
  html_notebook:
    theme: cerulean
    fig_caption: yes
references:
- id: enders2015applied
  title: Applied econometric time series
  author:
  - family: Enders
    given: Walter
  publisher: John Wiley \& Sons
  type: book
  issued:
    year: 2015
- id: da2011econometria
  title: Econometria de séries temporais
  author: 
  - family: Bueno
    given: Rodrigo De Losso da Silveira
  publisher: Cengage Learning
  type: book
  issued:
    year: 2011
- id: tsay2014introduction
  title: An introduction to analysis of financial data with R
  author:
  - family: Tsay
    given: Ruey S
  publisher: John Wiley \& Sons
  type: book
  issued:
    year: 2014
- id: campbell1997econometrics
  title: The econometrics of financial markets
  author:
  - family: Campbell
    given: John Y
  - family: Lo
    given: Andrew Wen-Chuan
  - family: MacKinlay
    given: Archie Craig
  publisher: Princeton (NJ) Princeton University Press
  type: book
  issued:
    year: 1997
- id: morettin2008econometria
  title: Econometria financeira um curso em séries temporais financeiras
  author:
  - family: Morettin
    given: Pedro Alberto
  publisher: Edgard Blucher
  type: book
  issued:
    year: 2008
nocite: | 
  @enders2015applied, @da2011econometria, @tsay2014introduction, @campbell1997econometrics, @morettin2008econometria
---

Este material tem como objetivo contribuir para o entendimento sobre séries temporais univariadas, focando nas **características de dados financeiros e suas propriedades**.  Para tanto, vamos análisar dois tipos de séries temporais financeiras: preços e retornos. Para cada tipo de série, estudaremos o seguinte:

* Preços de ativos financeiros
    * Tipos de dados: igualmente espaçados e irregularmente espaçados 
    * Propriedades da série temporal dos preços
* Retornos de ativos financeiros
    * Formas de calcular o retorno entre dois períodos consecutivos ($t$ e $t-1$) bem como para um intervalo de $k$ períodos ($t$ e $t-k$).
    * Propriedades da série temporal dos retornos (fatos estilizados)
    
Após entender os pontos acima, iniciamos a definição formal de **serie temporal univariada**, **processo estocástico** e **estacionariedade de um série temporal univariada**. Assim, entenderemos o motivo pelo qual a **série temporal dos retornos** é usada nos modelos de séries temporais univariadas ao **invés da série temporal dos preços**.

```{r, echo=FALSE}
suppressMessages(require(quantmod))
suppressMessages(require(DT))
suppressMessages(require(dplyr))
suppressMessages(require(magrittr))
suppressMessages(require(highcharter))
suppressMessages(require(PerformanceAnalytics))
```

<ol>
<li> <h4> <b> PREÇOS DE ATIVOS FINANCEIROS </b> </h2> </li>
</ol>

São dados que representam o valor pago por um ativo financeiro (ação, opção, título, ...) em um determinando instante do tempo. Em função de termos várias possibilidades para a períodicidade (minuto, hora, dia, semana, mes, semestre, ...) desta transação é preciso entender os diversos tipos de dados e suas propriedades.

* **DADOS IGUALMENTE ESPAÇADOS**

São aqueles em que o intervalo de tempo ($\Delta t$) entre observações consecutivas é constante. Por exemplo, os dias, semanas e meses. Para facilitar o entendimento, mostramos abaixo uma tabela com dados igualmente espaçados para a ação **GOOGL** da Alphabet Inc (Holding de empresas do Google).

```{r, echo=FALSE, tidy=TRUE, results='asis', warning=FALSE}
# Exemplo de dados igualmente espaçados
google_day <- quantmod::getSymbols("GOOGL", src = "yahoo", auto.assign=FALSE, from = "2018-07-02")
google_day <- data.table::as.data.table(round(google_day,2))
colnames(google_day) <- c("data", "abertura", "alta", "baixa", "fechamento", "volume", "ajustado")
DT::datatable(google_day, rownames = FALSE)
```

O **preço de abertura** é determinado pelo chamado **leilão de pré-abertura** que acontece 15 minutos antes do início do pregão (9h:45min). Neste leilão, todas as ordens de compra e venda de ações que já foram enviadas para a bolsa (antes da abertura), entram no leilão. O sistema da bolsa identifica as faixas de preços com maiores volumes de negociação. O preço que tiver maior número de compras e vendas de ações, será o preço de abertura do ativo neste dia. Durante o leilão nenhum negócio é fechado, mesmo que os valores de compra e venda coincidam. 

Ao final do leilão (10hs que é o início do pregão eletrônico), as negociações seguem até as 16hs:55min. Neste horário, inicia-se outro leilão (chamado de **call de fechamento**). O processo é parecido com o leilão de pré-abertura, porém o preço mais negociado na compra e na venda de um ativo, determinará o **preço de fechamento** daquela ação.

Já o **preço de fechamento ajustado** acontece quando uma ação, dentro de qualquer dia de negociação, tem o seu preço alterado para incluir quaisquer **distribuições de proventos** que ocorreram em qualquer momento antes da abertura do dia seguinte. Como utilidade prática, o preço de fechamento ajustado é uma ferramenta muito útil para examinar os resultados históricos de uma empresa, pois ele oferece aos analistas uma imagem precisa do valor patrimonial da empresa além do preço de mercado.

* **DADOS IRREGULARMENTE ESPAÇADOS**

Sabemos que dados financeiros podem ser observados em instantes de tempo irregularmente espaçados, como os dados intradiários de ativos negociados em bolsa de valores ou mercadorias, ou taxas de câmbio. 

Neste caso, os intervalos entre observações são **variáveis aleatórias** e podemos ter observações coincidindo num mesmo instante de tempo ou em intervalos distintos. Este tipo de dado é chamado de **alta frequência**. 

Abaixo, exemplo de dado intradiário para a ação **GOOG** da [Alphabet Inc](https://abc.xyz/) em cada minuto. Neste caso, não conseguimos visualizar a existência de observações coincidindo no mesmo instante de tempo ou em intervalos distintos em função do sistema coletar apenas uma transação por minuto, mas esperamos que durante o dia tal característica seja apresentada para este tipo de ativo.

```{r, echo=FALSE, results='asis', warning=FALSE}
google <- quantmod::getSymbols("GOOG", src="av", api.key = "51UXNHZBHN280H9R", auto.assign = FALSE, warnings = FALSE,
                   periodicity = "intraday", interval = "1min", from = "2018-08-10")
google <- data.table::as.data.table(round(google,2))
colnames(google) <- c("data", "abertura", "alta", "baixa", "fechamento", "volume")
DT::datatable(google, rownames = FALSE)
```

Para a disciplina em estudo, vamos usar **séries temporais originadas por dados regularmente espaçados**. Porém, existem outros campos da Econometria de Séries Temporais que concentram-se na análise de séries temporais originadas de dados irregularmente espaçados.


* **CARACTERÍSTICAS DE SÉRIES TEMPORAIS**

Já é bem documentado na literatura que **séries temporais** apresentam certas **características**, como:

* tendência;
* sazonalidade;
* pontos influentes (atípicos, também chamados de *outliers*);
* heterocedasticidade condicional (variância condicional não é constante);
* não-linearidade

De um modo geral, podemos dizer que uma série é **não-linear quando responde de maneira diferente a choques grandes ou pequenos, ou ainda, a choques negativos ou positivos**. Por exemplo, a queda do IBOVESPA pode ocasionar maior volatilidade no mercado do que a alta. 

Abaixo, exemplo de série temporal (dados do total de passageiros aéreos nos EUA entre 1949 e 1960) com **tendência** e **sazonalidade**. 

```{r, echo=FALSE, tidy=TRUE, results='asis', warning = FALSE}
data(AirPassengers)
hc <- highchart(type = "stock") %>% 
  hc_title(text = "Passageiros Aéreos / Mês") %>% 
  hc_subtitle(text = "Passageiros aéreos nos EUA entre 1949 e 1960") %>% 
  hc_add_series(AirPassengers, id = "ts", color = '#0d233a') %>%
  hc_exporting(enabled = TRUE) 
  
hc
```

O gráfico abaixo decompõe a série temporal do total de passageiros aéreos nos EUA entre 1949 e 1960 facilitando a visualização dos componentes de **sazonalidade** e **tendência**.

```{r, echo=FALSE, tidy=TRUE, results='asis', warning = FALSE}
decomp <- stl(AirPassengers, "per")
hchart(decomp) 
```


<ol start="2">
<li> <h4> <b> RETORNOS DE ATIVOS FINANCEIROS </b> </h2> </li>
</ol>

Muitos estudos financeiros fazem uso de **retornos de ativos ao invés dos seus preços**. Segundo [@campbell1997econometrics], isso acontece porque as séries de retornos são mais fáceis de manusear do que as séries de preços em função de possuírem propriedades estatísticas mais atrantes (como estacionariedade). Porém, existem muitas formas de mensurar o retorno de um ativo. 

Abaixo, mostramos exemplos de como obter o retorno de ativos. Para tanto, deixe $P_{t}$ ser o preço de um ativo em um dado período $t$.

* **RETORNO SIMPLES**

Assumindo que o ativo não paga dividendos, o retorno simples é o retorno obtido ao se manter o ativo adquirido em $(t-1)$ até $t$, ou seja:

$$
R_{t}=\frac{P_{t}}{P_{t-1}}-1 = \frac{P_{t}-P_{t-1}}{P_{t-1}}
$$
Para demonstração, usaremos a tabela abaixo que nos mostra dados da ação da **Microsoft Corporation**. Temos que o retorno de manter a ação de 13/02/18 até 14/02/2018 é $R_{t}=90.81/89.83-1\approx 0.0109$ de modo que o retorno simples correspondente é de $1.09\%$.

```{r, echo=FALSE, tidy=TRUE, results='asis', warning=FALSE}
# Exemplo de dados para cálculo de retorno
microsoft <- quantmod::getSymbols("MSFT", src = "yahoo", warnings = FALSE, auto.assign=FALSE, from = "2018-02-07", to = "2018-02-15")
microsoft <- data.table::as.data.table(round(microsoft,2))
colnames(microsoft) <- c("data", "abertura", "alta", "baixa", "fechamento", "volume", "ajustado")
DT::datatable(head(microsoft), rownames = FALSE)
```

Caso o ativo pague dividendos, o retorno simples é:

$$
R_{t} = \frac{P_{t}+D_{t}}{P_{t-1}} -1 
$$
Além disso, caso o objetivo seja um retorno acima do ativo livre de risco, o retorno simples se torna um retorno em excesso, conforme abaixo:

$$
Z_{t} = R_{t} - R_{0t}
$$
onde $R_{0t}$ é o retorno do ativo livre de risco. Outra alternativa é trabalhar com o logaritmo para obter o retorno entre dois períodos subsequentes (para períodos distantes o logaritmo não será uma boa aproximação do real retorno). Usar o logaritmo suavizará a série temporal dos retornos. Para tanto, podemos fazer uso do seguinte:

$$
R_{t} = \ln{\left(\frac{P_{t}}{P_{t-1}}\right)} = \ln(P_{t}) - \ln(P_{t-1})
$$

No gráfico abaixo, comparamos a diferença entre usar o retorno simples (em azul) e o retorno em logaritmo (em vermelho) para gerar a série temporal do retorno da ação IBM (nternational Business Machines) entre 01/01/2000 e 16/02/2018.

```{r, echo=FALSE, tidy=TRUE, results='asis', warning = FALSE}
ibm <- getSymbols("IBM", src = "yahoo", warnings = FALSE, from="2000-01-01", to="2018-02-16")
log <- PerformanceAnalytics::Return.calculate(IBM[,4], method = "log")
discreto <- PerformanceAnalytics::Return.calculate(IBM[,4], method = "discrete")

hc0 <- highchart(type = "stock") %>% 
  hc_title(text = "Comparação entre retorno discreto e em log da IBM") %>% 
  hc_subtitle(text = "Dados extraídos usando o pacote quantmod do R") %>% 
  hc_add_series(name = "logaritmo", log, id = "ts", color = "red") %>%
  hc_add_series(name = "discreto", discreto, id = "ts", color = "blue") %>%
  hc_exporting(enabled = TRUE)


hc0

```


* **RETORNO SIMPLES ENTRE MULTIPERÍODOS**

Suponha agora que o ativo é mantido por $k$ períodos entre as datas $t-k$ e $t$. Desta forma, o retorno bruto simples nos **k** períodos será:

$$
1+R_{t}[k]=\frac{P_{t}}{P_{t-k}}=\frac{P_{t}}{P_{t-1}}\times \frac{P_{t-1}}{P_{t-2}}\times ...\times \frac{P_{t-k+1}}{P_{t-k}}
$$

Assim, o retorno simples em **k** períodos é apenas o produto dos retornos simples para os **k** períodos envolvidos. Além disso, o retorno líquido nos **k** períodos será $R_{t}[k]=\frac{P_{t}}{P_{t-k}}-1=\frac{P_{t}-P_{t-k}}{P_{t-k}}$.

Para ilustrar, considere novamente os dados da ação da **Microsoft Corporation** da tabela anterior. Se avaliarmos de 12/02/2018 até 14/02/2018 temos que o retorno no período foi de $1+R_{t}[3] = 90.81/89.13 \approx 1,01884$ de modo que o retorno simples no período foi de $1.884\%$.

* **CARACTERÍSTICAS DA SÉRIE TEMPORAL DOS RETORNOS (FATOS ESTILIZADOS)**

Os **retornos financeiros** apresentam **características peculiares** diferentemente de muitas séries temporais. Retornos **raramente apresentam tendências ou sazonalidades**, com exceção eventualmente de retornos intradiários. Além disso, séries de taxas de câmbio e séries de taxas de juros podem apresentar tendências que variam no tempo. Por outro lado, é comumente encontrado em séries de retornos as seguintes características:

* estacionariedade: as propriedades estatísticas das séries são invariantes ao longo do tempo;
* fraca dependência linear e não linear: a série normalmente é pouco ou não autocorrelacionada, mas a série do quadrado dos retornos é autocorrelacionada (isto implica que independente de quando ocorrer um grande retorno em valor absoluto, existe uma boa chance do próximo retorno também ser grande em valor absoluto);
* caudas pesadas na distribuição e excesso de curtose: a série dos retornos geralmente não é Gaussiana (distribuição Normal), o que está interligado ao fato dos retornos ao quadradado serem autocorrelacionados 
* comportamento heterocedástico condicional.

O comportamento heterocedástico reúne características como **aglomerados de volatilidade** e efeitos alavanca, que aponta para o efeito de choque. **Choques negativos normalmente afetam a volatilidade condicional em maior magnitude do que choques positivos**, ou seja, uma queda no retorno implica uma volatilidade condicional alta.

Vamos analisar a série temporal do  Standard & Poor's 500. Temos dois gráficos sendo o primeiro o fechamento do preço da índice para todos os dias com negociação entre 01/01/2000 e 16/02/2018 enquanto no segundo apresentamos o retorno simples obtido conforme metodololgia exposta anteriormente. 


```{r, echo=FALSE, tidy=TRUE, results='asis', warning = FALSE}
sp <- getSymbols("^GSPC", src = "yahoo", warnings = FALSE, from="2000-01-01", to="2018-02-16")
hc1 <- highchart(type = "stock") %>% 
  hc_title(text = "Fechamento S&P 500") %>% 
  hc_subtitle(text = "Dados extraídos usando o pacote quantmod do R") %>% 
  hc_add_series(GSPC[,4], id = "ts") %>%
  hc_exporting(enabled = TRUE)
  
hc1

```

Notamos que os fatos estilizados apontados anteriormente estão presentes na série de retornos do **S&P 500** dado que há aparente estacionariedade, média ao redor de zero e agrupamentos de volatilidade. Períodos de alta volatilidade coincidem com épocas nas quais ocorreram crises em diversos países que influenciaram o mercado financeiro. 

```{r, echo=FALSE, tidy=TRUE, results='asis', warning = FALSE}
retorno <- PerformanceAnalytics::Return.calculate(GSPC[,4])
hc2 <- highchart(type = "stock") %>% 
  hc_title(text = "Retorno S&P 500") %>% 
  hc_subtitle(text = "Dados extraídos usando o pacote quantmod do R") %>% 
  hc_add_series(retorno, id = "ts", color = '#0d233a') %>%
  hc_exporting(enabled = TRUE)

hc2

```

<ol start="3">
<li> <h4> <b> DEFINIÇÃO DE SÉRIE TEMPORAL E SUA RELAÇÃO COM PROCESSO ESTOCÁSTICO </b> </h2> </li>
</ol>

Uma série temporal é uma sequência de observações ${\{{y}_{t}\}}$ indexadas pela data de cada observação (minuto, hora, dia, semana, mês, trimestre, semestre, ano, ...).


$$
{\{{y}_{t}\}}_{t=-\infty}^{\infty}=\{...,{y}_{t-1},{y}_{0},{y}_{1},{y}_{2},...,{y}_{t},...\} 
$$

   * Cada observação é tratada como uma variável aleatória com uma distribuição de probabilidade;
   * Uma coleção de variáveis aleatórias ordenadas no tempo é chamada de **processo estocástico**;
   * **Hipótese:** Se a distribuição de $y_{t}$ é a mesma para todos os valores de $t$, então dizemos que a série é estacionária. Esta é a definição de **estacionariedade forte**. 


<ol start="4">
<li> <h4> <b> DEFINIÇÃO DE ESTACIONARIEDADE </b> </h2> </li>
</ol>

Uma série temporal é dita estacionária se ela se desenvolve no tempo aleatoriamente ao redor de uma média constante, refletindo alguma forma de equilíbrio estável. Formalmente, temos que uma série temporal de retornos de um ativo financeiro será fracamente estacionária se:

   * $E[r_{t}]=\mu$, 
   * $Var(r_{t}) = \gamma_{0}$ 
   * $Cov(r_{t},r_{t-l})=\gamma_{l}$ 

onde $\mu$ e $\gamma_{0}$ são constantes e $\gamma_{l}$ é função de uma defasagem $l$ qualquer, mas não do tempo $t$. Abaixo, para cada condição apresentada um gráfico com séries que atendem e que violam tais condições.

   * Média:
<p align="center">
![Alt text](../../../figures/Mean_nonstationary.png)
</p>

   * Variância:
<p align="center">
![Alt text](../../../figures/Var_nonstationary.png)
</p>

   * Covariância:
<p align="center">
![Alt text](../../../figures/Cov_nonstationary.png)
</p>

#### **REFERÊNCIAS**