<center> <h3> <b> ECONOMETRIA DE SÉRIES TEMPORAIS </b> </h3> </center> 

Neste repositório você encontrará informações e códigos necessários para gerar o material da disciplina Econometria II ministrada no curso de Ciências Econômicas do [Ibmec-MG](https://www.ibmec.br/mg).

Tal disciplina cobre os principais modelos econométricos para tratamento de séries temporais univariadas e multivariadas tanto estacionárias quanto não-estacionárias. O objetivo do curso é capacitar os alunos a lidarem com tais modelos, dominando os métodos de estimação, avaliação e previsão.

A disciplina usa a linguagem de programação R de forma que os alunos, ao final do curso, consigam aplicar os métodos discutidos em sala de aula a problemas reais de economia e finanças. 

Os tópicos discutidos são os seguintes:

* [Introdução à Econometria](https://rpubs.com/hudsonchavs/introducao) 
    * Teoria econômica e econometria (Modelo Econômico x Modelo Econométrico)
    * Relação entre a estrutura de dados econômicos e abordagem econométrica a ser utilizada
* Programação em R 
    * Como usar as plataformas DataCamp e RStudio Cloud para aprender o R  
    * [Introdução: Aritmética, Variáveis, Vetores, Fatores, Matrizes, Data Frames e Listas](http://rpubs.com/hudsonchavs/introducaoR)
    * [Intermediário: Operadores Relacionais, Condições, Loops, Funções, Datas e Pacotes](https://rpubs.com/hudsonchavs/intermediarioR)
    * [Importar dados locais (csv, xlsx, txt) e da internet (csv, xlsx, txt, APIs)](http://rpubs.com/hudsonchavs/importardadosR)
    * [Coletar dados financeiros e econômicos de fontes públicas como o Banco Central do Brasil, BM&F Bovespa, IBGE, IPEA, etc](http://rpubs.com/hudsonchavs/coletardadosR)
* Revisão de Estatística
    * [Probabilidade, Variáveis Aleatórias, Distribuições de Probabilidade Univariadas, Vetores Aleatórios e Distribuições de Probabilidade Multivariadas](https://rpubs.com/hudsonchavs/revisaoestatistica)
    * [O Princípio da Máxima Verossimilhança](http://rpubs.com/hudsonchavs/principiomaximaverossimilhanca)
* Séries temporais univariadas
    * [Características de dados financeiros, suas propriedades e definição de uma série temporal univariada](http://rpubs.com/hudsonchavs/seriestemporaisunivariadas)
    * [Correlação, Autocorrelação e Autocorrelação Parcial](http://rpubs.com/hudsonchavs/fac_facp)
    * [Modelos Autorregressivos (AR)](http://rpubs.com/hudsonchavs/modeloar)
    * [Modelos de Médias Móveis (MA)](http://rpubs.com/hudsonchavs/modeloma)
    * [Modelos Autorregressivos e de Médias Móveis (ARMA)](http://rpubs.com/hudsonchavs/modeloarma)
    * [Estimação de Modelos ARMA](http://rpubs.com/hudsonchavs/estimacaoarma)
    * [Raíz Unitária e Modelos ARIMA](https://rpubs.com/hudsonchavs/ruarima)
    * [Sazonalidade e Modelos SARIMA](https://rpubs.com/hudsonchavs/sarima)
    * [Modelos de Heterocedasticidade Condicional - ARCH e GARCH](https://rpubs.com/hudsonchavs/archgarch)
* Séries temporais multivariadas
    * [Definição de uma série temporal multivariada](https://rpubs.com/hudsonchavs/seriesmultivariadas)
    * [Modelo Vetorial Autorregressivo (VAR)](https://rpubs.com/hudsonchavs/varsvar)
    * [Efeito Causal Dinâmico (Modelos DLM e ADL)](https://rpubs.com/hudsonchavs/dlmadl)
    * [Cointegração, Modelo de Correção de Erros (MCE) e Modelo Vetorial de Correção de Erros (MVCE)](https://rpubs.com/hudsonchavs/vec)

### **PRÁTICA**

Além deste material, você pode ter acesso aos códigos em R usados em sala de aula para mostrar na prática o processo de estimação dos modelos estudados durante o curso. Para isso, crie uma conta no [RStudio Cloud](https://rstudio.cloud/) e conecte ao projeto da disciplina disponível neste [link](https://rstudio.cloud/project/53159). 

### **ESTRUTURA DE PASTAS DESTE REPOSITÓRIO**

Para facilitar o entendimento, os arquivos estão divididos em:

* `notes`: pasta com os códigos que geram as notas de aula publicadas no [RPubs](https://rpubs.com/hudsonchavs/)
* `homework`: pasta com códigos que geram as listas de exercícios e trabalhos

