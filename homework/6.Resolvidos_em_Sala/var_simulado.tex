\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color} % color is loaded by hyperref
\hypersetup{unicode=true,
            colorlinks=true,
            linkcolor=Maroon,
            citecolor=Blue,
            urlcolor=blue,
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\providecommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{\large{\textbf{ECONOMETRIA II - Exercícios VAR}}}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{\scriptsize{Hudson Chaves Costa}}
    \preauthor{\centering\large\emph}
  \postauthor{\par}
    \date{}
    \predate{}\postdate{}
  
\usepackage{titling}
\usepackage{multirow}
\pretitle{\begin{center}\small\includegraphics[height=1.2cm]{../../figures/ibmecLogo.png}\\[\bigskipamount]}
\posttitle{\end{center}}

\begin{document}
\maketitle

\hypertarget{processo-de-estimacao-e-analise-de-modelos-var}{%
\subparagraph{\texorpdfstring{\textbf{PROCESSO DE ESTIMAÇÃO E ANÁLISE DE
MODELOS
VAR:}}{PROCESSO DE ESTIMAÇÃO E ANÁLISE DE MODELOS VAR:}}\label{processo-de-estimacao-e-analise-de-modelos-var}}

Para o caso onde temos uma base de dados de \(k\) séries temporais e
queremos estimar um modelo \textbf{VAR(p)} puramente estatístico ou em
forma reduzida, os seguintes passos são necessários:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Visualizar os dados e identificar observações fora do padrão
  (outliers, sazonalidade, tendência).
\item
  Se necessário, transformar os dados para estabilizar a variância
  (logaritmo, retornos, variação ou retirar sazonalidade, por exemplo)
\item
  Avaliar a função de correlação cruzada para confirmar a possibilidade
  de modelagem multivariada.
\item
  Testar se os dados são estacionários. Caso tenha raiz unitária é
  preciso diferenciar os dados até se tornarem estacionários. Neste
  caso, usamos os dados diferenciados no restante do processo.
\item
  Definir a ordem \(p\) para os dados em análise por meio de critérios
  de informação (escolher modelo com menor AIC, por exemplo)
\item
  Estimar o modelo escolhido no passo anterior.
\item
  Verificar significância estatística do modelo estimado e, caso seja
  necessário, eliminar parâmetros não significantes.

  \begin{itemize}
  \tightlist
  \item
    Para tanto, uma alternativa é usar o teste de causalidade de Granger
  \end{itemize}
\item
  Examinar se os resíduos se comportam como ruído branco e condições de
  estacionariedade do modelo. Caso contrário, retornar ao passo 4 ou 5.

  \begin{itemize}
  \tightlist
  \item
    Verificar a autocorrelação serial por meio da FAC e FACP dos
    resíduos de cada equação do modelo estimado. O ideal é não ter
    defasagens significativas.
  \item
    Verificar correlação cruzada por meio da FCC dos resíduos.
  \item
    Analisar a estabildiade do modelo estimado através dos autovalores
    associados ao mesmo.
  \item
    Avaliar a distribuição de probabilidade dos resíduos (se comportam
    conforme a distribuição assumida)
  \item
    Verificar a presença de heterocedasticidade condicional nos resíduos
  \end{itemize}
\item
  Uma vez que os resíduos são ruído branco e o modelo é estável:

  \begin{itemize}
  \tightlist
  \item
    Analisar funções de resposta ao impulso
  \item
    Analisar a importância das variáveis para explicar a variância do
    erro de previsão de cada variável
  \item
    Fazer previsões paras as variáveis do modelo
  \end{itemize}
\end{enumerate}

\hypertarget{exemplo-1-var-simulado}{%
\subparagraph{\texorpdfstring{\textbf{EXEMPLO 1: VAR
SIMULADO}}{EXEMPLO 1: VAR SIMULADO}}\label{exemplo-1-var-simulado}}

Suponha que conhecemos a priori o seguinte modelo VAR(1) composto por
duas séries temporais (\(r_{1t}\) e \(r_{2t}\)):

\[
\left[\begin{matrix} {r}_{1t} \\ {r}_{2t} \end{matrix} \right] = \left[\begin{matrix} 0.5 \\ 1.0 \end{matrix} \right] + \left[ \begin{matrix} 0.7 \\ 0.2 \end{matrix}\begin{matrix} 0.4 \\ 0.3 \end{matrix} \right] \left[\begin{matrix} {r}_{1,t-1} \\ {r}_{2,t-1} \end{matrix} \right] + \left[\begin{matrix} {a}_{1t} \\ {a}_{2t} \end{matrix} \right] 
\] Usando tal modelo, podemos simular dados para as duas séries. O
gráfico abaixo mostra as duas séries temporais resultantes.

\includegraphics{var_simulado_files/figure-latex/unnamed-chunk-1-1.pdf}

Suponha agora, que não sabemos os verdadeiros valores dos parâmetros do
modelo VAR(1) para as duas séries e que queremos estimar um modelo
\(VAR(p)\) e avaliar seus resultados. Seguindo os passos mostrados
anteriormente, temos:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  O gráfico acima mostra que não há distorções incoerentes nas duas
  séries. Pelo contrário, elas se comportam semelhantemente durante o
  período.
\item
  Como não encontramos nada de errado com os dados vamos mantê-los no
  formato que estão.
\item
  Uma vez que temos os dados processados, é necessário avaliar a
  possibilidade de usar uma abordagem multivariada. Para tanto, além da
  teoria econômica, a função de correlação cruzada pode ser usada.
  Abaixo, os gráficos mostram que há relação intertemporal entre as duas
  séries.
\end{enumerate}

\includegraphics{var_simulado_files/figure-latex/unnamed-chunk-2-1.pdf}
4. O próximo passo é testar a estacionariedade das séries. As tabelas
abaixo, apresentam os resultados do teste de raíz unitária ADF.

\begin{table}[h!]
\centering
\caption{Teste de Raíz Unitária para a série $r1$}
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Especificação}       & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Passeio Aleatório                       & -3.2073   & 0.01                            \\
Passeio Aleatório com drift             & -8.4677    & 0.01                            \\
Passeio Aleatório com drift e tendência & -8.5357   & 0.01                            \\ \hline
\end{tabular}
\end{table}

\begin{table}[h!]
\centering
\caption{Teste de Raíz Unitária para a série $r2$}
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Especificação}       & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Passeio Aleatório                       & -3.7053   & 0.01                            \\
Passeio Aleatório com drift             & -11.1629    & 0.01                            \\
Passeio Aleatório com drift e tendência & -11.2109   & 0.01                            \\ \hline
\end{tabular}
\end{table}

Como resultado temos que a estatística do teste para a série r1 é
-3.2073 enquanto que para a série r2 foi -3.7053 (considerando um
passeio aleatório simples). Em ambos os casos o p-valor foi de 0.01.
Assim, a hipótese nula de raiz unitária pode ser rejeitada ao nível de
significância de 5\% (pois 0,05\textgreater{}0,01) e ambas as séries são
estacionárias. Em função disso, não precisamos diferenciá-las para o
próximo passo.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Anteriormente, discutimos a utilidade dos critérios de informação para
  a verificação da ordem p do modelo VAR. Aqui, sabemos que se trata de
  um VAR(1). Vamos avaliar o resultado do teste e confirmar se ele
  conclui que para os dados simulados seria melhor fazer uso de um
  VAR(1).
\end{enumerate}

\begin{table}[ht]
\centering
\begin{tabular}{rrrrrrr}
  \hline
 & 1 & 2 & 3 & 4 & 5 & 6 \\ 
  \hline
AIC(n) & -0.01 & -0.01 & -0.00 & 0.00 & 0.01 & 0.01 \\ 
  HQ(n) & -0.00 & 0.01 & 0.02 & 0.03 & 0.05 & 0.06 \\ 
  SC(n) & 0.01 & 0.04 & 0.07 & 0.09 & 0.11 & 0.14 \\ 
  FPE(n) & 0.99 & 0.99 & 1.00 & 1.00 & 1.01 & 1.01 \\ 
   \hline
\end{tabular}
\caption{Critérios de Informação x Defasagens}
\end{table}

Os resultados mostram que para todos os critérios de informação
concluímos que trata-se de um modelo VAR(1) conforme simulação (valor 1
na primeira tabela do resultado). A defasagem ótima é sempre aquela para
a qual o critério apresenta o menor valor. Assim, podemos observar que o
uso destes critérios podem ajudar na especificação da ordem \(p\) do
modelo VAR.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{5}
\tightlist
\item
  Uma vez que temos a ordem selecionada para nosso conjunto de dados,
  podemos estimar seus parâmetros.
\end{enumerate}

\begin{table}[h!]
\begin{center}
\caption{VAR(1)}
\begin{tabular}{l c c }
\hline
 & Equação 1 & Equação 2 \\
\hline
r1.l1      & $0.67 \; (0.03)^{***}$ & $0.19 \; (0.02)^{***}$ \\
r2.l1      & $0.46 \; (0.06)^{***}$ & $0.33 \; (0.04)^{***}$ \\
const      & $0.52 \; (0.11)^{***}$ & $1.01 \; (0.08)^{***}$ \\
\hline
R$^2$      & 0.75                   & 0.46                   \\
Adj. R$^2$ & 0.75                   & 0.46                   \\
Num. obs.  & 999                    & 999                    \\
RMSE       & 1.38                   & 0.99                   \\
\hline
\multicolumn{3}{l}{\scriptsize{$^{***}p<0.001$, $^{**}p<0.01$, $^*p<0.05$}}
\end{tabular}
\label{table:coefficients}
\end{center}
\end{table}

A partir dos resultados acima podemos observar que os coeficientes
estimados estão bem próximos dos verdadeiros valores assim como a matriz
de variância e covariância dos resíduos.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{6}
\item
  Em função de todos os coeficientes estimados serem estatisticamente
  significantes, optamos por não aplicar qualquer restrição sobre as
  equações estimadas.
\item
  A partir da estimação do modelo, seguimos naturalmente aos testes de
  diagnóstico, a fim de verificar se os resíduos satisfazem as hipóteses
  feitas pelo método de estimação. Nesse sentido, avaliamos a presença
  de correlação residual (FAC e FACP) bem como a estabilidade dos
  parâmetros (condição necessária mostrada anteriormente). Além disso,
  verificamos a correlação cruzada (FCC) entre os resíduos, a presença
  de heterocedasticidade condicional e se os resíduos seguem a
  distribuição assumida. Caso as hipóteses não sejam atendidas, é
  necessário retornar ao passo 3 ou 4.
\end{enumerate}

\includegraphics{var_simulado_files/figure-latex/unnamed-chunk-6-1.pdf}

\includegraphics{var_simulado_files/figure-latex/unnamed-chunk-7-1.pdf}

Como é possível observar, para ambos os termos de erro de cada variável
não temos resíduos com autocorrelação (a partir da não-significância
estatística das defasagens das funções FAC e FACP).

Já a correlação cruzada entre os resíduos das duas equações pode ser
verificada pela Função de Correlação Cruzada (FCC). O gráfico mostra que
nosso modelo controlou a relação dinâmica (defasagens de ambas as séries
impactando umas às outras além delas mesmas) entre as duas séries. O
ideal é não ter diversas defasagens estatísticamente significantes, mas
apenas para \(l=0\) ou defasagens pequenas.

\includegraphics{var_simulado_files/figure-latex/unnamed-chunk-8-1.pdf}

Para verificar a estabildiade do modelo estimado podemos obter os
autovalores associados e esperamos que eles sejam inferiores a 1 em
valor absoluto, conforme derivação das propriedades de um modelo VAR(p)
estudadas anteriormente. Dado que o modelo é bivariado, a saída retorna
dois autovalores ambos menores que 1 em módulo.

\begin{verbatim}
## [1] 0.8376824 0.1589111
\end{verbatim}

Já a presença de heterocedasticidade condicional é avaliada por meio do
teste ARCH-LM que tem a hipótese nula de homocedasticidade dos resíduos.

\begin{verbatim}
## 
##  ARCH (multivariate)
## 
## data:  Residuals of VAR object modelo
## Chi-squared = 37.23, df = 45, p-value = 0.7882
\end{verbatim}

O resultado mostra que não é possível rejeitar a hipótese nula ao nível
de significância de 5\textbackslash{}5 dado que o p-valor do teste é de
0.7882.

Por fim, a hipótese de normalidade dos resíduos de cada equação estimada
é avaliada. Como é possível observar pelos p-valor do teste de
Jarque-Bera, não foi possível rejeitar a hipótese nula de normalidade
dos resíduos ao nível de significância de 5\%.

\begin{verbatim}
## $r1
## 
##  JB-Test (univariate)
## 
## data:  Residual of r1 equation
## Chi-squared = 1.0397, df = 2, p-value = 0.5946
## 
## 
## $r2
## 
##  JB-Test (univariate)
## 
## data:  Residual of r2 equation
## Chi-squared = 0.13559, df = 2, p-value = 0.9345
\end{verbatim}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{8}
\tightlist
\item
  Uma análise útil para avaliar as interações das variáveis presentes em
  um modelo VAR é a função impulso-resposta, também conhecida como
  resposta ao impulso. A interpretação desta análise se inicia assumindo
  um choque de uma unidade em uma variável com a respectiva resposta em
  outra variável. Abaixo, os gráficos desta análise:
\end{enumerate}

\includegraphics{var_simulado_files/figure-latex/unnamed-chunk-12-1.pdf}

A interpretação é bastante direta: um choque de uma unidade na variável
\(r1\) gera uma pertubação exponencialmente decrescente na variável
\(r2\) (lado esquerdo da figura acima), que desaparece completamente por
volta do período 25. Já \(r2\) tem um efeito crescente em \(r1\) nos
primeiros dois períodos (lado direiro da figura acima), a partir daí
caindo e desaparecendo também em torno do período 25.

Outra análise que os modelos VAR proporcionam é analisar a importância
das variáveis para explicar a variância do erro de previsão de cada
variável.

\includegraphics{var_simulado_files/figure-latex/unnamed-chunk-13-1.pdf}

Em linhas gerais, a função retorna quanto da variação (percentual) do
erro de previsão é atribuído a cada variável para uma sequência de
valores no tempo. Ou seja, a variância total dos erros de previsão é
decomposta, no nosso caso, em duas componentes, uma relacionada à
\(r_1\) e a outra à \(r_2\). Na prática, essa análise nos ajuda a
verificar quais variáveis são realmente importantes quando o objetivo é
realizar previsões. Quanto maior for a contribuição percentual de uma
variável para a variação total de outra, mais importante ela é para
realizarmos boas previsões da variável da qual realizamos a
decomposição.

Assim, na decomposição da variância do nosso modelo, verificamos que por
volta de 50\% dos desvios da previsão de \(r_2\) em relação aos valores
observados se devem às oscilações de \(a_1\) que é o erro de \(r_1\). Já
para \(r_1\) menos que 5\% dos desvios da previsão de \(r_1\) em relação
aos valores observados se devem às oscilações de \(a_2\) que é o erro de
de \(r_2\). Tais efeitos eram esperados, dado que a parte estocástica de
\(r_2\) depende fortemente dos choques estruturais em \(r_1\), como
ressaltamos quando apresentamos o processo.


\end{document}
