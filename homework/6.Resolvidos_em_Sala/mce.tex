\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color} % color is loaded by hyperref
\hypersetup{unicode=true,
            colorlinks=true,
            linkcolor=Maroon,
            citecolor=Blue,
            urlcolor=blue,
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\providecommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{\large{\textbf{ECONOMETRIA II - Exercícios MCE}}}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{\scriptsize{Hudson Chaves Costa}}
    \preauthor{\centering\large\emph}
  \postauthor{\par}
    \date{}
    \predate{}\postdate{}
  
\usepackage{titling}
\usepackage{multirow}
\usepackage{float}
\pretitle{\begin{center}\small\includegraphics[height=1.2cm]{../../figures/ibmecLogo.png}\\[\bigskipamount]}
\posttitle{\end{center}}

\begin{document}
\maketitle

\hypertarget{modelos-de-correcao-de-erros-mce}{%
\subparagraph{\texorpdfstring{\textbf{MODELOS DE CORREÇÃO DE ERROS
(MCE)}}{MODELOS DE CORREÇÃO DE ERROS (MCE)}}\label{modelos-de-correcao-de-erros-mce}}

Um dos objetivos da Econometria é avaliar empiricamente teorias
econômicas que, em geral, pressupõem relações de equilíbrio de longo
prazo entre variáveis. Esta averiguação pode ser feita com base em
modelagem de séries temporais que, via de regra, apresentam algum tipo
de tendência.

Neste sentido, fazer uso de modelos de regressão que envolvem dados de
séries temporais apresentando tendência pode gerar resultados
inconsistentes. Isso acontece porque as técnicas tradicionais de
regressão, tal como \textbf{Mínimos Quadrados Ordinários (MQO)},
precisam que as variáveis sejam estacionárias, ou seja, que a média e
variância não oscilem com o tempo e que a covariância não dependa do
tempo.

Como alternativa, surge o \textbf{Modelo de Correção de Erros (MCE)} que
utiliza a análise de cointegração para a estimação de modelos quando as
variáveis não apresentam estacionariedade. A idéia intuitiva de
\textbf{cointegração} é que variáveis não estacionárias podem caminhar
juntas, isto é, podem ter trajetórias temporais interligadas, de forma
que no longo prazo apresentem relação de equilíbrio.

\hypertarget{processo-de-estimacao-de-modelos-mce}{%
\subparagraph{\texorpdfstring{\textbf{PROCESSO DE ESTIMAÇÃO DE MODELOS
MCE:}}{PROCESSO DE ESTIMAÇÃO DE MODELOS MCE:}}\label{processo-de-estimacao-de-modelos-mce}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Visualizar os dados e identificar observações fora do padrão
  (outliers, sazonalidade, tendência)
\item
  Se necessário, transformar os dados para estabilizar a variância
  (logaritmo ou retirar sazonalidade, por exemplo)
\item
  Testar se as séries temporais são cointegradas:

  \begin{itemize}
  \tightlist
  \item
    Testar se as séries são estacionárias.

    \begin{itemize}
    \tightlist
    \item
      Se não estacionárias estimar a relação entre as séries (modelo de
      regressão linear)
    \item
      Se estacionárias, não faz sentido testar cointegração e deve-se
      estimar modelo de regressão linear tradicional
    \end{itemize}
  \item
    Obter os resíduos de cointegração a partir da regressão linear do
    passo anterior (caso raiz unitária)
  \item
    Testar se os resíduos são estacionários

    \begin{itemize}
    \tightlist
    \item
      Se não estacionários implica que as séries não são cointegradas
    \item
      Se estacionário implica que as séries são cointegradas
    \end{itemize}
  \end{itemize}
\item
  Estimar o modelo de correção de erros e obter a estimativa para os
  termos de curto e longo prazo
\item
  Avaliar os resíduos da estimação:

  \begin{itemize}
  \tightlist
  \item
    Verificar a autocorrelação serial por meio da FAC e FACP dos
    resíduos do modelo estimado. O ideal é não ter defasagens
    significativas
  \item
    Verificar a estacionariedade dos resíduos do modelo MCE
  \end{itemize}
\end{enumerate}

\hypertarget{exemplo-1-expectativa-de-inflacao-e-ipca}{%
\subparagraph{\texorpdfstring{\textbf{EXEMPLO 1: EXPECTATIVA DE INFLAÇÃO
E
IPCA}}{EXEMPLO 1: EXPECTATIVA DE INFLAÇÃO E IPCA}}\label{exemplo-1-expectativa-de-inflacao-e-ipca}}

Desde 2005, o Instituto Brasileiro de Economia (IBRE/FGV) inclui na
Sondagem do Consumidor uma pergunta quantitativa sobre a expectativa de
inflação individual para os próximos 12 meses. São coletados dados de
mais de 2100 brasileiros nas principais capitais do país (Porto Alegre,
São Paulo, Rio de Janeiro, Belo Horizonte, Salvador, Brasília e Recife).

A pergunta quantitativa possui a seguinte formulação: na sua opinião, de
quanto será a inflação brasileira nos próximos 12 meses?

Nosso objetivo é estimar um modelo de correção de erro (MCE) baseado no
procedimento de Engle-Granger, com a expectativa de inflação sendo
explicada pelo Índice de Preços ao Consumidor Amplo (IPCA/IBGE). Assim,
queremos avaliar se tais variáveis são cointegradas e qual é a taxa de
ajuste quando ocorre um desvio da média. Para tal, devemos executar as
etapas apresentadas anteriormente.

Os dados aplicados no exemplo pertencem ao período entre setembro de
2005 e dezembro de 2013 e são parcialmente ilustrados na tabela abaixo:

\begin{table}[!htbp] \centering 
  \caption{} 
  \label{} 
\begin{tabular}{@{\extracolsep{5pt}} cccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
data & expectativa & ipca & focus \\ 
\hline \\[-1.8ex] 
2005-09-01 & $9.440$ & $6.040$ & $4.770$ \\ 
2005-10-01 & $9.500$ & $6.360$ & $4.720$ \\ 
2005-11-01 & $9.130$ & $6.220$ & $4.630$ \\ 
2005-12-01 & $9.070$ & $5.690$ & $4.510$ \\ 
2006-01-01 & $8.210$ & $5.700$ & $4.570$ \\ 
2006-02-01 & $8.260$ & $5.510$ & $4.510$ \\ 
\hline \\[-1.8ex] 
\end{tabular} 
\end{table}

Na sequência, executamos o processo proposto:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Visualizar os dados e identificar observações fora do padrão
  (outliers, sazonalidade, tendência)
\end{enumerate}

\includegraphics{mce_files/figure-latex/unnamed-chunk-2-1.pdf}

Aparentemente, não existe dados fora do padrão temporal da série. Assim,
não faz sentido tratar outliers, sazonalidade ou tendência.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Se necessário, transformar os dados para estabilizar a variância
  (logaritmo ou retirar sazonalidade, por exemplo)
\end{enumerate}

Em função do item anterior, não é preciso fazer transformações nos
dados.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  Testar se as séries temporais são cointegradas
\end{enumerate}

Para testar se as séries são cointegradas, utilizaremos o teste
\textbf{Engle-Granger} que consiste em ajustar uma relação entre as
variáveis e realizar o teste de raiz unitária de Dickey-Fuller Aumentado
(ADF) nos resíduos da equação ajustada. Para o nosso caso, temos o
seguinte processo:

\begin{itemize}
\tightlist
\item
  Executar o teste de raiz unitária para \(expectativa\) e \(ipca\) e
  certificar que elas são \(I(1)\). Se elas forem \(I(0)\) não há razão
  para testar cointegração.
\item
  Estimar a relação
  \(expectativa_t = \alpha + \beta ipca_t + \epsilon_t\) e obter
  \(\hat{\epsilon}_t\)
\item
  Testar se os resíduos são estacionários, ou seja, se são \(I(0)\)
  usando o teste de Dickey-Fuller Aumentado \textbf{(a equação do teste
  não deve ter intercepto nem tendência porque os resíduos de MQO
  oscilam em torno de zero)}, conforme abaixo:
\end{itemize}

\[
\begin{aligned}
& \Delta \hat{\epsilon}_t = \phi\hat{\epsilon}_{t-1}+\sum_{i=1}^{p-1}{\lambda_{i}\Delta \hat{\epsilon}_{t-i}}+\xi_t \\
& \\
& H_0: \phi=0~\rightarrow \hat{\epsilon}_t~\text{não estacionário}~\rightarrow expectativa~\text{e}~ipca~\text{não são cointegradas} \\
& H_1: \phi<0~\rightarrow \hat{\epsilon}_t~\text{estacionário}~\rightarrow expectativa~\text{e}~ipca~\text{são cointegradas} \\
\end{aligned}
\]

Como resultado, temos a tabela abaixo.

\begin{table}[h!]
\centering
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Série Temporal}  & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Expectativa de Inflação             & -0.4558           & 0.4679        \\
IPCA                                & -0.1976    & 0.5501 \\
$\hat{\epsilon}$                    & -3.8383       & 0.01    \\ \hline
\end{tabular}
\caption{Teste ADF para as séries temporais e resíduo OLS}
\end{table}

É possível observar que tanto a série temporal da expectativa de
inflação quanto a série temporal do IPCA são \(I(1)\), ou seja,
apresentam raiz unitária. Isso é mostrado pelo alto p-valor apresentado
para o teste em ambas as séries, o que indica que não é possível
rejeitar a hipótese nula de raiz unitária.

Assim, estimar um modelo de regressão linear simples usando as duas
variáveis em nível
(\(expectativa_t = \alpha + \beta ipca_t + \epsilon_t\)) produzirá
resultados que não são consistentes, uma vez que os resíduos serão
autocorrelacionados (\textbf{regressão espúria}). Uma alternativa seria
usar as duas séries em primeira diferença e estimar o modelo:

\[
\Delta expectativa_t = \beta \Delta ipca_t + \upsilon_t
\] dado que \(\Delta expectativa_t\) e \(\Delta ipca_t\) serão
estacionários. Porém, isso pode esconder as propriedades de longo prazo
da relação entre as duas variáveis caso exista cointegração entre as
duas séries. Em função disso, devemos avaliar se as duas séries são
cointegradas, ou seja, se apresentam alguma relação de longo prazo.

Para tanto, executamos o teste de Engle-Granger por meio do teste de
raiz unitária sobre os resíduos de um modelo de regressão linear simples
entre as duas séries. O resultado do teste mostra que há uma relação de
longo prazo entre as séries dado que o p-valor do teste é \(<0,5\)
indicando que é possível rejeitar a hipótese nula de raiz unitária ao
nível de 5\% de significância. Isso indica que \(\hat{\epsilon}\) é
\(I(0)\).

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  Estimar o modelo de correção de erros e obter a estimativa para os
  termos de curto e longo prazo
\end{enumerate}

Dada a cointegração entre as duas séries o modelo que devemos estimar
com objetivo de obter tanto o componente de curto prazo quanto o
componente de longo prazo é:

\[
\begin{aligned}
& \Delta expectativa_t = \gamma \Delta ipca_t -\lambda(expectativa_{t-1} -\alpha - \beta ipca_{t-1}) + \varepsilon_t \\
& \Delta expectativa_t = \gamma \Delta ipca_t -\lambda(\hat{\epsilon}_{t-1}) + \varepsilon_t \\
\end{aligned}
\] que deixa claro que a variação na expectativa de inflação é explicada
por dois componentes: \textbf{um termo de curto prazo e outro de longo
prazo}. As variações no IPCA, \(\Delta ipca_t\), representam o termo de
curto prazo e \(\hat{\epsilon}_{t-1}\) é o componente de longo prazo,
pois pode ser entendido como o termo de erro da regressão
\(expectativa_t = \alpha + \beta ipca_t + \epsilon_t\) que nada mais é
que uma combinação linear estacionária entre as duas variáveis e indica
quão rápido o ajustamento ocorre em um período. Se o parâmetro
(\(\lambda\)) for próximo de 0, o ajustamento ocorre lentamente enquanto
que próximo de -1 o ajustamento é rápido.

A tabela abaixo apresenta o resultado do modelo de correção de erro
(MCE) bem como o modelo de regressão linear simples (OLS). Como
esperado, há uma relação de longo prazo estatisticamente significante
entre as duas variáveis. Porém, o ajustamento dos desvios de curto prazo
é lento em um período. Isso mostra que desvios na inflação corrente
medido pelo IPCA impactarão levemente nas expectativas no próximo
período.

\begin{table}[h!]
\begin{center}
\begin{tabular}{l c c }
\hline
 & OLS & MCE \\
\hline
(Intercept) & $4.63 \; (0.25)^{***}$ &                       \\
ipca        & $0.63 \; (0.05)^{***}$ &                       \\
d(ipca, 1)  &                        & $0.26 \; (0.15)$      \\
L(resid, 1) &                        & $-0.21 \; (0.08)^{*}$ \\
\hline
R$^2$       & 0.66                   & 0.09                  \\
Adj. R$^2$  & 0.66                   & 0.07                  \\
Num. obs.   & 100                    & 98                    \\
RMSE        & 0.50                   & 0.39                  \\
\hline
\multicolumn{3}{l}{\scriptsize{$^{***}p<0.001$, $^{**}p<0.01$, $^*p<0.05$}}
\end{tabular}
\caption{Comparativo Modelos}
\label{table:coefficients}
\end{center}
\end{table}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Avaliar os resíduos da estimação:
\end{enumerate}

Por fim, devemos avaliar os resíduos das duas estimações. Para tanto,
usamos a função de autocorrelação serial (FAC) dos resíduos de cada
modelo bem como o teste de raiz unitária ADF. Os resultados mostram que
os dois resíduos são estacionários. Isso está indicado pelo p-valor do
teste para cada resíduo.

Porém, ao avaliarmos a autocorrelação serial dos resíduos encontramos
que os resíduos do OLS apresentam várias defasagens estatisticamente
significantes, o que é um forte indício de autocorrelação serial nos
resíduos. Por outro lado, para os resíduos do MCE há apenas uma
defasagem significante o que mostra que a autocorrelação em sua grande
maioria foi controlada pelo modelo. Talvez, seria intessante usar mais
dados em função da amostra ser relativamente pequena para uma série
temporal (100 observações) ou estimar o modelo com mais defasagens e
escolher o modelo com menor critério de informação.

\includegraphics{mce_files/figure-latex/unnamed-chunk-5-1.pdf}
\includegraphics{mce_files/figure-latex/unnamed-chunk-5-2.pdf}

\begin{table}[h!]
\centering
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Série Temporal}  & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Resíduo OLS             & -3.8223           & 0.01        \\
Resíduo MCE             & -7.2835       & 0.01    \\ \hline
\end{tabular}
\caption{Teste ADF para os Resíduos}
\end{table}

\hypertarget{exemplo-2-pairs-trading}{%
\subparagraph{\texorpdfstring{\textbf{EXEMPLO 2: PAIRS
TRADING}}{EXEMPLO 2: PAIRS TRADING}}\label{exemplo-2-pairs-trading}}

\emph{Pairs trading} é uma estratégia que ficou famosa em Wall Street
nos anos 80. Segundo Gatev, Goetzmann, and Rouwenhorst N (2006), a
origem da estratégia está ligada a Nunzio Tartaglia e um grupo de
acadêmicos que trabalhavam na época no banco de investimentos Morgan
Stanley. Reunidos, descobriram oportunidades para obter lucros através
de programas automatizados para negociação de ações, que utilizavam
técnicas estatísticas avançadas para a época.

Segundo Vidyamurthy (2004) uma das técnicas utilizadas era a
identificação de pares de ações cujos preços \textbf{moviam juntos} e
sempre que uma anormalidade na relação histórica de preços entre essas
ações fosse detectada, os pares eram negociados sob a premissa de que
essa anormalidade iria se corrigir automaticamente. Tartaglia e seu
grupo empregaram a estratégia de \emph{pairs trading} com grande sucesso
ao longo de 1987. Apesar de o grupo ter sido desmontado em 1989, a
estratégia pairs trading se tornou cada vez mais popular entre traders
individuais, investidores institucionais e hedge-funds.

Estratégias de arbitragem estatística são baseadas em encontrar uma
série temporal que possua a característica de estacionariedade ou
reversão à média. Isto significa que é possível identificar situações em
que a série divergiu de seu comportamento histórico e prever com alguma
segurança que a série convergirá ou reverterá para um comportamento
``médio''. O conceito de cointegração formaliza matematicamente este
comportamento e permite a realização de testes estatísticos para
detectar séries com este comportamento.

Assim, os passos para operar com pares de ações são:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Selecionar duas ações que movem similarmente
\item
  Monitorar as diferenças entre as duas ações no curto prazo

  \begin{itemize}
  \tightlist
  \item
    No longo prazo podemos usar cointegração para fazer esse
    monitoramento
  \end{itemize}
\item
  Regra de Decisão:

  \begin{itemize}
  \tightlist
  \item
    Vender a ação com preço alto e comprar a ação com preço baixo
  \item
    Outra opção é usar o \emph{spread} entre as duas ações
  \end{itemize}
\end{enumerate}

Neste exemplo, vamos aplicar a teoria de cointegração para definir uma
estratégia de operação na bolsa de valores usando como base as ações
\textbf{VALE3} e \textbf{PETR4}. Assim, se existir uma relação de
cointegração entre as séries de preços dos dois ativos, significa que
pode ser possível realizar operações lucrativas de arbitragem. Por outro
lado, se o par não for cointegrado, será impossível encontrar uma
relação consistente para operar o par.

\includegraphics{mce_files/figure-latex/unnamed-chunk-6-1.pdf}

O gráfico abaixo mostra que há uma relação entre as duas séries
selecionadas para nosso teste. Basicamente, temos o seguinte modelo de
correção de erros (MCE) a ser estimado:

\[
\begin{aligned}
& \Delta vale_t = \gamma \Delta itau_t -\lambda(vale_{t-1} -\alpha - \beta itau_{t-1}) + \varepsilon_t \\
& \Delta vale_t = \gamma \Delta itau_t -\lambda(\hat{\epsilon}_{t-1}) + \varepsilon_t \\
\end{aligned}
\] onde \(\epsilon_{t-1}\) é spread entre as duas ações, ou seja,
\(spread_t = vale_t - \alpha -\beta itau_t\). Se spread for muito alto,
compra-se \(itau_t\) e vende-se \(vale_t\). Se spread for muito baixo,
compra-se \(vale_t\) e vende-se \(itau_t\).

Porém, como sabemos, só faz sentido usar o modelo de correção de erros
(MCE) se as duas séries possuem raiz unitária e o resíduo (aqui, o
\emph{spread}) da estimação OLS entre as duas séries for estacionário.
Assim, a tabela abaixo apresenta o resultado do teste:

\begin{table}[h!]
\centering
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Série Temporal}  & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
VALE3                               & -0.5536           & 0.4399        \\
PETR4                               & -1.2138    & 0.2295 \\
spread                              & -4.0687       & 0.01    \\ \hline
\end{tabular}
\caption{Teste ADF para as séries temporais e resíduo OLS}
\end{table}

Como resultado, temos que as duas séries não são estacionárias enquanto
que o \emph{spread} é estacionário. Assim, é justificável fazer uso do
modelo MCE. A tabela abaixo, mostra o resultado da estimação tanto do
modelo OLS quanto do MCE e podemos observar que a velocidade do ajuste
entre as duas séries é bastante pequeno.

\begin{table}[h!]
\begin{center}
\begin{tabular}{l c c }
\hline
 & OLS & MCE \\
\hline
(Intercept)        & $4.59 \; (0.39)^{***}$ &                        \\
PETR4              & $1.68 \; (0.02)^{***}$ &                        \\
d(PETR4, 1)        &                        & $0.69 \; (0.03)^{***}$ \\
L(resid\_pairs, 1) &                        & $-0.01 \; (0.00)^{**}$ \\
\hline
R$^2$              & 0.76                   & 0.17                   \\
Adj. R$^2$         & 0.76                   & 0.17                   \\
Num. obs.          & 2165                   & 2163                   \\
RMSE               & 6.21                   & 0.74                   \\
\hline
\multicolumn{3}{l}{\scriptsize{$^{***}p<0.001$, $^{**}p<0.01$, $^*p<0.05$}}
\end{tabular}
\caption{Modelo OLS e MCE}
\label{table:coefficients}
\end{center}
\end{table}

Por fim, podemos visualizar o gráfico do \emph{spread}:

\includegraphics{mce_files/figure-latex/unnamed-chunk-9-1.pdf}

\hypertarget{referencias}{%
\paragraph{REFERÊNCIAS}\label{referencias}}
\addcontentsline{toc}{paragraph}{REFERÊNCIAS}

\hypertarget{refs}{}
\leavevmode\hypertarget{ref-gatev2006pairs}{}%
Gatev, Evan, Goetzmann, and Rouwenhorst N. 2006. ``Pairs Trading
Performance of a Relative-Value Arbitrage Rule.'' \emph{Nature
Materials} 19 (3). Oxford University Press: 797--827.

\leavevmode\hypertarget{ref-vidyamurthy2004pairs}{}%
Vidyamurthy, Ganapathy. 2004. \emph{Pairs Trading Quantitative Methods
and Analysis}. John Wiley \& Sons.


\end{document}
