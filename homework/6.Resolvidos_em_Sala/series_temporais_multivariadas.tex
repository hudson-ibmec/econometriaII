\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color} % color is loaded by hyperref
\hypersetup{unicode=true,
            colorlinks=true,
            linkcolor=Maroon,
            citecolor=Blue,
            urlcolor=blue,
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\providecommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{\large{\textbf{ECONOMETRIA II - Exercícios Séries Temporais Multivariadas}}}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{\scriptsize{Hudson Chaves Costa}}
    \preauthor{\centering\large\emph}
  \postauthor{\par}
    \date{}
    \predate{}\postdate{}
  
\usepackage{titling}
\usepackage{multirow}
\pretitle{\begin{center}\small\includegraphics[height=1.2cm]{../../figures/ibmecLogo.png}\\[\bigskipamount]}
\posttitle{\end{center}}

\begin{document}
\maketitle

\hypertarget{serie-temporal-multivariada}{%
\paragraph{\texorpdfstring{\textbf{SÉRIE TEMPORAL
MULTIVARIADA:}}{SÉRIE TEMPORAL MULTIVARIADA:}}\label{serie-temporal-multivariada}}

Deixe \(\boldsymbol{r_{t}}=(r_{1t},r_{2t},...,r_{kt})^{'}\) ser o
logaritmo dos retornos de \(k\) ativos no tempo \(t\), onde
\(\boldsymbol{r^{'}}\) denota a transposta de \(\boldsymbol{r}\).
Estatisticamente falando, trata-se de um vetor aleatório com \(k\)
variáveis aleatórias.

Aprenderemos modelos econométricos que analisam o processo multivariado
\(\boldsymbol{r_{t}}\) de forma que seja possível estudar a relação
entre os componentes de \(\boldsymbol{r_{t}}\) e aumentar a acurácia na
previsão \(\boldsymbol{r_{t+h}}\) para \(h>0\).

Suponha que temos uma série temporal multivariada com dois componentes,
ou seja, \(k=2\). Assim, temos:

\[
\begin{aligned}
& \boldsymbol{r_{t}}=(r_{1t},r_{2t})^{'} \\
& \\
& \boldsymbol{\mu} = E(\boldsymbol{r_{t}}) = \left[ \begin{matrix} E\left( { r }_{ 1t } \right)  \\ E\left( { r }_{ 2t } \right)  \end{matrix} \right]  \\
& \\
& \boldsymbol{\Gamma_{0}} = Cov(\boldsymbol{r_{t}}) = \left[\begin{matrix} E\left[\left({r}_{1t}-{\mu}_{1}\right) \left({r}_{1t}-{\mu}_{1} \right)\right] & E\left[\left({r}_{1t}-{\mu}_{1} \right)\left({r}_{2t}-{\mu}_{2} \right) \right] \\ E\left[\left({r}_{2t}-{\mu}_{2} \right) \left({r}_{1t}-{\mu}_{1} \right)\right] & E\left[\left({r}_{2t}-{\mu}_{2} \right) \left({r}_{2t}-{\mu}_{2} \right)\right] \end{matrix} \right] = \left[\begin{matrix} Var\left({r}_{1t}\right) & Cov\left({r}_{1t},{r}_{2t}\right) \\ Cov\left({r}_{2t},{r}_{1t}\right) & Var\left({r}_{2t}\right) \end{matrix} \right] \\
& \\
& \boldsymbol{\Gamma}_{1} = Cov(\boldsymbol{r}_{t},\boldsymbol{r}_{t-1}) = \left[\begin{matrix} E\left[\left({r}_{1t}-{\mu}_{1} \right) \left({r}_{1,t-1}-{\mu}_{1} \right)\right] & E\left[\left({r}_{1t}-{\mu}_{1} \right) \left({r}_{2,t-1}-{\mu}_{2} \right) \right] \\ E\left[\left({r}_{2t}-{\mu}_{2} \right) \left({r}_{1,t-1}-{\mu }_{1} \right) \right] & E\left[\left({r}_{2t}-{\mu}_{2} \right) \left({r}_{2,t-1}-{\mu}_{2} \right) \right]  \end{matrix} \right] = \left[\begin{matrix} Cov\left({r}_{1t}, r_{1,t-1}\right) & Cov\left({r}_{1t},{r}_{2,t-1}\right) \\ Cov\left({r}_{2t},{r}_{1,t-1}\right) & Cov\left({r}_{2t},{r}_{2,t-1}\right) \end{matrix} \right] \\
& \\
& \boldsymbol{\Gamma}_{2} = Cov(\boldsymbol{r}_{t},\boldsymbol{r}_{t-2}) = \left[\begin{matrix} E\left[\left({r}_{1t}-{\mu}_{1} \right) \left({r}_{1,t-2}-{\mu}_{1} \right)\right] & E\left[\left({r}_{1t}-{\mu}_{1} \right) \left({r}_{2,t-2}-{\mu}_{2} \right) \right] \\ E\left[\left({r}_{2t}-{\mu}_{2} \right) \left({r}_{1,t-2}-{\mu }_{1} \right) \right] & E\left[\left({r}_{2t}-{\mu}_{2} \right) \left({r}_{2,t-2}-{\mu}_{2} \right) \right]  \end{matrix} \right] = \left[\begin{matrix} Cov\left({r}_{1t}, r_{1,t-2}\right) & Cov\left({r}_{1t},{r}_{2,t-2}\right) \\ Cov\left({r}_{2t},{r}_{1,t-2}\right) & Cov\left({r}_{2t},{r}_{2,t-2}\right) \end{matrix} \right] \\
& \\
& \begin{matrix} . \\ . \\ . \end{matrix} \\
& \\
& \boldsymbol{\Gamma}_{l} = Cov(\boldsymbol{r}_{t},\boldsymbol{r}_{t-L}) = \left[ \begin{matrix} E\left[ \left({r}_{1t}-{\mu}_{1} \right) \left({r}_{1,t-l}-{\mu}_{1} \right)  \right]  & E\left[\left({r}_{1t}-{\mu}_{1} \right) \left({r}_{2,t-l}-{\mu}_{2} \right)  \right]  \\ E\left[\left({r}_{2t}-{\mu}_{2} \right)\left({r}_{1,t-l}-{\mu}_{1} \right)\right]  & E\left[\left({r}_{2t}-{\mu}_{2} \right) \left({r}_{2,t-l}-{\mu}_{2} \right)  \right]  \end{matrix} \right] = \left[\begin{matrix} Cov\left({r}_{1t}, r_{1,t-l}\right) & Cov\left({r}_{1t},{r}_{2,t-l}\right) \\ Cov\left({r}_{2t},{r}_{1,t-l}\right) & Cov\left({r}_{2t},{r}_{2,t-l}\right) \end{matrix} \right]
\end{aligned}
\] Portanto, para \(l=0,1,2,...,l\) e sendo \(i=1,2\) a linha da matriz
e \(j=1,2\) sua coluna, temos:

\begin{itemize}
\tightlist
\item
  \(\Gamma_{l,ii}\) é a autocovariância de cada ativo
\item
  \(\Gamma_{l,ij}\) é a covariância entre \(r_{it}\) e \(r_{j,t-l}\) que
  quantifica a dependência linear de \(r_{it}\) e \(r_{j,t-l}\).
\end{itemize}

\break

\textbf{MATRIZ DE CORRELAÇÃO CRUZADA}

Deixe \(\boldsymbol{D}\) ser uma matriz \(k \times k\) diagonal
consistindo dos desvios-padrão de \(r_{it}\) para \(i=1,...,k\). Para
\(k=2\) (nosso exemplo anterior), temos:

\[
\begin{aligned}
& \boldsymbol{D} = \left[\begin{matrix} \sqrt{\Gamma_{11}(0)} & 0 \\ 0 & \sqrt{\Gamma_{22}(0)} \end{matrix} \right] = \left[\begin{matrix} DP(r_{1t}) & 0 \\ 0 & DP(r_{2t}) \end{matrix} \right] \\
& \\
& \boldsymbol{\rho_{0}} = \boldsymbol{D^{-1}}\boldsymbol{\Gamma_{0}}\boldsymbol{D^{-1}} = \left[\begin{matrix} Corr(r_{1t},r_{1t}) & Corr(r_{1t},r_{2t}) \\ Corr(r_{2t},r_{1t}) & Corr(r_{2t},r_{2t}) \end{matrix} \right] = \left[\begin{matrix} 1 & Corr(r_{1t},r_{2t}) \\ Corr(r_{2t},r_{1t}) & 1 \end{matrix} \right] \\
& \\
& \boldsymbol{\rho_{1}} = \boldsymbol{D^{-1}}\boldsymbol{\Gamma_{1}}\boldsymbol{D^{-1}} = \left[\begin{matrix} Corr(r_{1t},r_{1,t-1}) & Corr(r_{1t},r_{2,t-1}) \\ Corr(r_{2t},r_{1,t-1}) & Corr(r_{2t},r_{2,t-1}) \end{matrix} \right] \\
& \\
& \boldsymbol{\rho_{2}} = \boldsymbol{D^{-1}}\boldsymbol{\Gamma_{2}}\boldsymbol{D^{-1}} = \left[\begin{matrix} Corr(r_{1t},r_{1,t-2}) & Corr(r_{1t},r_{2,t-2}) \\ Corr(r_{2t},r_{1,t-2}) & Corr(r_{2t},r_{2,t-2}) \end{matrix} \right] \\
& \\
& \begin{matrix} . \\ . \\ . \end{matrix} \\
& \\
& \boldsymbol{\rho_{l}} = \boldsymbol{D^{-1}}\boldsymbol{\Gamma_{l}}\boldsymbol{D^{-1}} = \left[\begin{matrix} Corr(r_{1t},r_{1,t-l}) & Corr(r_{1t},r_{2,t-l}) \\ Corr(r_{2t},r_{1,t-l}) & Corr(r_{2t},r_{2,t-l}) \end{matrix} \right] \\
\end{aligned} 
\]

Em geral, a relação linear entre duas séries temporais (\(r_{1t}\) e
\(r_{2t}\)) pode ser resumida da seguinte forma:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \(r_{1t}\) e \(r_{2t}\) não tem dependência linear se
  \(\rho_{12}(l)=\rho_{21}(l)=0\) para todo \(l \geq 0\).
\item
  \(r_{1t}\) e \(r_{2t}\) são correlacionados contemporâneamente se
  \(\rho_{12}(0)\neq0\).
\item
  \(r_{1t}\) e \(r_{2t}\) não tem dependência em defasagens se
  \(\rho_{12}(l)=0\) e \(\rho_{21}(l)=0\) para todo \(l>0\). Neste caso
  dizemos que as duas séries são desacopladas.
\item
  Existe relação unidirecional de \(r_{1t}\) para \(r_{2t}\) se
  \(\rho_{12}(l)=0\) para todo \(l>0\), mas \(\rho_{21}(v)\neq0\) para
  algum \(v>0\). Neste caso, \(r_{1t}\) não depende de qualquer valor
  passado de \(r_{2t}\), mas \(r_{2t}\) depende de algum valor passado
  de \(r_{1t}\).
\item
  Existe uma relação de \emph{feedback} entre \(r_{1t}\) para \(r_{2t}\)
  se \(\rho_{12}(l)\neq0\) para algum \(l>0\) e \(\rho_{21}(v)\neq0\)
  para algum \(v>0\).
\end{enumerate}

OBSERVAÇÕES:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Em geral \(\boldsymbol{\Gamma}_{l}\) não é simétrica, exceto para
  \(l=0\).
\item
  \(\boldsymbol{\Gamma}_{l}=\boldsymbol{\Gamma}_{-l}^{'}\) o que implica
  em \(Cor(r_{1t},r_{2,t-1})=Cor(r_{2t},r_{1,t+1})\).
\end{enumerate}

\hypertarget{matriz-de-correlacoes-cruzada-exemplo}{%
\paragraph{\texorpdfstring{\textbf{MATRIZ DE CORRELAÇÕES CRUZADA:
EXEMPLO}}{MATRIZ DE CORRELAÇÕES CRUZADA: EXEMPLO}}\label{matriz-de-correlacoes-cruzada-exemplo}}

Empiricamente, sabemos que é dificil absorver simultaneamente muitas
matrizes de correlação cruzada. Em função disso, para verificar a
dependência linear entre séries temporais consideraremos um gráfico que
é equivalente à matriz de correlação cruzada.

Para cada \((i,j)\)-ésima posição da matriz de correlação cruzada
amostral, temos o gráfico de \(\hat{\rho}_{l,ij}\) contra \(l\) para
\(l = \pm1, \pm2, ..., \pm m\), onde \(m\) é um inteiro. Este gráfico é
uma generalização da função de autocorrelação (FAC) de uma série
temporal univariada.

Para uma série multivariada de dimensão \(k\), teremos \(k^{2}\)
gráficos. Assim como no caso da FAC, para facilitar o entendimento dos
gráficos temos uma linha pontilhada indicando se a correlação cruzada na
defasagem é estatisticamente significante ou não.

Usando como exemplo as ações \textbf{PETR4}, \textbf{VALE3} e
\textbf{ITUB4.SA}, (ou seja, \(k=3\)), temos abaixo os gráficos da
correlação cruzada amostral para cada uma das ações em relação a ela
mesma e as demais. Assim, na diagonal principal temos a função de
autocorrelação (FAC) da ação e nas demais posições as correlações
cruzadas.

\includegraphics{series_temporais_multivariadas_files/figure-latex/unnamed-chunk-1-1.pdf}


\end{document}
