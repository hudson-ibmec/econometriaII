\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color} % color is loaded by hyperref
\hypersetup{unicode=true,
            colorlinks=true,
            linkcolor=Maroon,
            citecolor=Blue,
            urlcolor=blue,
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\providecommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{\large{\textbf{ECONOMETRIA II}}}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{\scriptsize{Hudson Chaves Costa}}
    \preauthor{\centering\large\emph}
  \postauthor{\par}
    \date{}
    \predate{}\postdate{}
  
\usepackage{titling}
\usepackage{multirow}
\usepackage{float}
\pretitle{\begin{center}\small\includegraphics[height=1.2cm]{../../figures/ibmecLogo.png}\\[\bigskipamount]}
\posttitle{\end{center}}

\begin{document}
\maketitle

\hypertarget{processo-de-estimacao-e-analise-de-modelos-var}{%
\subparagraph{\texorpdfstring{\textbf{PROCESSO DE ESTIMAÇÃO E ANÁLISE DE
MODELOS
VAR:}}{PROCESSO DE ESTIMAÇÃO E ANÁLISE DE MODELOS VAR:}}\label{processo-de-estimacao-e-analise-de-modelos-var}}

Para o caso onde temos uma base de dados de \(k\) séries temporais e
queremos estimar um modelo \textbf{VAR(p)} puramente estatístico ou em
forma reduzida, os seguintes passos são necessários:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Visualizar os dados e identificar observações fora do padrão
  (outliers, sazonalidade, tendência).
\item
  Se necessário, transformar os dados para estabilizar a variância
  (logaritmo ou retirar sazonalidade, por exemplo)
\item
  Avaliar a função de correlação cruzada para confirmar a possibilidade
  de modelagem multivariada.
\item
  Testar se os dados são estacionários. Caso tenha raiz unitária é
  preciso diferenciar os dados até se tornarem estacionários
\item
  Definir a ordem \(p\) para os dados em análise por meio de critérios
  de informação (escolher modelo com menor AIC, por exemplo)
\item
  Estimar o modelo escolhido no passo anterior.
\item
  Verificar significância estatística do modelo estimado e, caso seja
  necessário, eliminar parâmetros não significantes.

  \begin{itemize}
  \tightlist
  \item
    Para tanto, uma alternativa é usar o teste de causalidade de Granger
  \end{itemize}
\item
  Examinar se os resíduos se comportam como ruído branco e condições de
  estacionariedade do modelo. Caso contrário, retornar ao passo 4 ou 5.

  \begin{itemize}
  \tightlist
  \item
    Verificar a autocorrelação serial por meio da FAC e FACP dos
    resíduos de cada equação do modelo estimado. O ideal é não ter
    defasagens significativas.
  \item
    Verificar correlação cruzada por meio da FCC dos resíduos.
  \item
    Analisar a estabildiade do modelo estimado através dos autovalores
    associados ao mesmo.
  \item
    Avaliar a distribuição de probabilidade dos resíduos (se comportam
    conforme a distribuição assumida)
  \item
    Verificar a presença de heterocedasticidade condicional nos resíduos
  \end{itemize}
\item
  Uma vez que os resíduos são ruído branco e o modelo é estável:

  \begin{itemize}
  \tightlist
  \item
    Analisar funções de resposta ao impulso
  \item
    Analisar a importância das variáveis para explicar a variância do
    erro de previsão de cada variável
  \item
    Fazer previsões paras as variáveis do modelo
  \end{itemize}
\end{enumerate}

\hypertarget{exemplo-2-um-modelo-var-usado-pelo-bacen}{%
\subparagraph{\texorpdfstring{\textbf{EXEMPLO 2: UM MODELO VAR USADO
PELO
BACEN}}{EXEMPLO 2: UM MODELO VAR USADO PELO BACEN}}\label{exemplo-2-um-modelo-var-usado-pelo-bacen}}

Historicamente, os modelos VAR utilizados pelo Banco Central do Brasil
(BACEN) estão divididos em dois grandes grupos: \textbf{VAR com
fundamentação econômica (chamado de VAR Estrutural ou SVAR) e VAR
puramente estatístico (sem restrição estrutural de ordem econômica e
conhecido como forma reduzida ou VAR)}.

Em ambos os casos, os modelos VAR geram previsões de inflação para os
preços livres. As previsões de inflação do Índice Nacional de Preços ao
Consumidor Amplo (IPCA) cheio são obtidas pela \textbf{combinação das
previsões da inflação de preços livres do VAR com as previsões da
inflação de preços administrados, que são estimadas de forma
independente}. Tais resultados são utilizados pelo Copom para a tomada
de decisão sobre a taxa de juros (Selic).

Como exemplo, vamos estimar um modelo VAR usando dados mensais de
01/2000 a 10/2018 das seguintes séries temporais:

\begin{itemize}
\tightlist
\item
  \textbf{preços livres:} preços que oscilam em função das condições de
  oferta e demanda dos produtos. Usamos o Índice Nacional de Preços ao
  Consumidor Amplo (ICPA) para itens livres, medido mensalmente e em
  variação percentual.
\item
  \textbf{preços administrados:} preços que são menos sensíveis às
  condições de oferta e de demanda porque são estabelecidos por contrato
  ou por órgão público (energia elétrica, planos de saúde, \ldots{}).
  Como esses contratos preveem, muitas vezes, reajustes de acordo com a
  inflação passada, pode-se afirmar que essa indexação parcial à
  inflação ocorrida torna esses preços efetivamente ``dependentes do
  passado'' e pouco sensíveis ao ciclo econômico. Usamos o Índice
  Nacional de Preços ao Consumidor Amplo (PCA) para itens monitorados,
  medido mensalmente e em variação percentual.
\item
  \textbf{câmbio:} média mensal da compra de Dólar americano (taxa de
  câmbio livre)
\item
  \textbf{juros reais:} taxa Selic acumulada no mês anualizada
  descontada a taxa de inflação medida pelo IGP-DI. É a taxa média
  ajustada dos financiamentos diários apurados no Sistema Especial de
  Liquidação e de Custória (Selic) para títulos federais (remuneração
  das instituições financeiras nas operações com títulos públicos)
\end{itemize}

Tal modelo foi escolhido usando-se como base documentação
disponibilizada pelo BACEN em 2012 que divulgou modelos usados para
previsão da inflação. Você pode acessar o documento usando este
\href{https://www.bcb.gov.br/htms/relinf/port/2012/09/ri201209b8p.pdf}{link}.

Na sequência, executamos o processo apresentado anteriormente:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Visualizando os gráficos abaixo, percebemos que há outliers nos
  períodos de crises, tanto nacional quanto internacional. Assim, uma
  alternativa seria adicionar uma variável dummy para cada uma das
  crises com o objetivo de diminuir o impacto destes outliers na
  estimação. Para tanto, duas dummies foram criadas sendo que para a
  dummy das eleições de 2002 foi adotado o período de 09/2002 até
  11/2002 enquanto que a dummy da crise financeira de 2008 usamos o
  período de 11/2008 a 12/2008.
\end{enumerate}

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-1-1.pdf}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Uma vez que adicionamos as variáveis dummy optamos por não estabilizar
  a variância dado que as demais observações parecem bem comportadas
\item
  Uma vez que temos os dados processados, temos que avaliar a
  possibilidade de usar uma abordagem multivariada. Para tanto, além da
  teoria econômica, a função de correlação cruzada pode ser usada.
  Abaixo, os gráficos mostram que há relação intertemporal entre as duas
  séries. Assim, faz sentido usar um modelo multivariado para avaliar a
  dependência entre as séries.
\end{enumerate}

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-2-1.pdf}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  O próximo passo é testar a estacionariedade das séries. Abaixo,
  mostramos os resultados:
\end{enumerate}

\begin{table}[h!]
\centering
\caption{Teste de Raíz Unitária para os Preços Livres}
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Especificação}       & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Passeio Aleatório                       & -3.284   & 0.01                            \\
Passeio Aleatório com drift             & -6.193    & 0.01                            \\
Passeio Aleatório com drift e tendência & -6.2687   & 0.01                            \\ \hline
\end{tabular}
\end{table}

\begin{table}[h!]
\centering
\caption{Teste de Raíz Unitária para os Preços Administrados}
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Especificação}       & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Passeio Aleatório                       & -5.6999   & 0.01                            \\
Passeio Aleatório com drift             & -8.4667    & 0.01                            \\
Passeio Aleatório com drift e tendência & -8.6158   & 0.01                            \\ \hline
\end{tabular}
\end{table}

\begin{table}[h!]
\centering
\caption{Teste de Raíz Unitária para variação no Câmbio Nominal}
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Especificação}       & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Passeio Aleatório                       & -8.3934   & 0.01                            \\
Passeio Aleatório com drift             & -8.4296    & 0.01                            \\
Passeio Aleatório com drift e tendência & -8.4044   & 0.01                            \\ \hline
\end{tabular}
\end{table}

\begin{table}[h!]
\centering
\caption{Teste de Raíz Unitária para a variação nos Juros Reais}
\begin{tabular}{lll}
\hline
\multicolumn{1}{c}{Especificação}       & \multicolumn{1}{c}{Estatística do Teste} & \multicolumn{1}{c}{P-valor} \\ \hline
Passeio Aleatório                       & -9.8151   & 0.01                            \\
Passeio Aleatório com drift             & -9.8524    & 0.01                            \\
Passeio Aleatório com drift e tendência & -9.8286   & 0.01                            \\ \hline
\end{tabular}
\end{table}

Em todas as séries temporais em análise o p-valor foi de 0.01. Assim, a
hipótese nula de raiz unitária pode ser rejeitada ao nível de
significância de 5\% (pois 0,05\textgreater{}0,01) e todas as séries são
estacionárias. Em função disso, não precisamos diferenciar as séries
para o próximo passo.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Anteriormente, discutimos a utilidade dos critérios de informação para
  a verificação da ordem p do modelo VAR. Abaixo, a tabela com os
  critérios de informação para diversas defasagens do modelo:
\end{enumerate}

\begin{table}[h!]
\centering
\begin{tabular}{rrrrrrr}
  \hline
 & 1 & 2 & 3 & 4 & 5 & 6 \\ 
  \hline
AIC(n) & -16.01 & -15.96 & -15.93 & -15.90 & -15.83 & -15.82 \\ 
  HQ(n) & -15.83 & -15.69 & -15.55 & -15.43 & -15.25 & -15.15 \\ 
  SC(n) & -15.58 & -15.28 & -15.00 & -14.73 & -14.40 & -14.15 \\ 
  FPE(n) & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 \\ 
   \hline
\end{tabular}
\caption{Critérios de Informação x Defasagens}
\end{table}

Os resultados mostram que para todos os critérios de informação o modelo
a ser estimado para os dados em análise é o VAR(1).

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{5}
\tightlist
\item
  Uma vez que temos a ordem selecionada para nosso conjunto de dados,
  podemos estimar seus parâmetros. Os resultados são apreentados na
  tabela 6.
\end{enumerate}

\begin{table}[H]
\begin{center}
\begin{tabular}{l c c c c }
\hline
 & Equação 1 & Equação 2 & Equação 3 & Equação 4 \\
\hline
Livre.l1         & $0.68 \; (0.05)^{***}$ & $0.10 \; (0.14)$       & $-0.02 \; (0.01)^{*}$  & $0.08 \; (0.01)^{***}$  \\
Administrados.l1 & $0.05 \; (0.02)^{*}$   & $0.34 \; (0.06)^{***}$ & $0.00 \; (0.00)$       & $0.01 \; (0.01)^{*}$    \\
VarCambio.l1     & $0.99 \; (0.46)^{*}$   & $3.39 \; (1.35)^{*}$   & $0.38 \; (0.07)^{***}$ & $-0.02 \; (0.12)$       \\
VarJurosReais.l1 & $-0.66 \; (0.26)^{*}$  & $1.54 \; (0.75)^{*}$   & $0.09 \; (0.04)^{*}$   & $0.03 \; (0.06)$        \\
const            & $0.11 \; (0.03)^{***}$ & $0.35 \; (0.08)^{***}$ & $0.01 \; (0.00)$       & $-0.05 \; (0.01)^{***}$ \\
criseeleicao     & $0.68 \; (0.15)^{***}$ & $0.93 \; (0.44)^{*}$   & $0.02 \; (0.02)$       & $-0.04 \; (0.04)$       \\
crisemundial     & $-0.20 \; (0.18)$      & $-0.66 \; (0.52)$      & $0.00 \; (0.03)$       & $0.07 \; (0.05)$        \\
\hline
R$^2$            & 0.60                   & 0.21                   & 0.18                   & 0.25                    \\
Adj. R$^2$       & 0.59                   & 0.19                   & 0.16                   & 0.23                    \\
Num. obs.        & 224                    & 224                    & 224                    & 224                     \\
RMSE             & 0.24                   & 0.70                   & 0.04                   & 0.06                    \\
\hline
\multicolumn{5}{l}{\scriptsize{$^{***}p<0.001$, $^{**}p<0.01$, $^*p<0.05$}}
\end{tabular}
\caption{Modelo VAR(1) BACEN}
\label{table:coefficients}
\end{center}
\end{table}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{6}
\tightlist
\item
  A partir dos resultados obtidos no passo anterior, percebemos que há
  parâmetros não significantes estatisticamente. Em função disso, vamos
  restringir nosso modelo VAR(1) eliminando variáveis ou parâmetros que
  prejudicam o ajuste do modelo. Para tanto, vamos executar o teste de
  causalidade de Granger para confirmar ou não a relação entre as
  variáveis e justificar a inclusão delas no VAR(1).
\end{enumerate}

Este teste examina se defasagens de uma variável auxiliam na previsão de
outra variável. Dessa maneira, o teste não nos informa nada a respeito
de causalidade em termos literais, mas oferece evidências estatísticas
de que oscilações passadas de uma variável estão correlacionadas com as
de uma outra.

Como exemplo, suponha que queremos verificar se defasagens nos Juros
Reais estão correlacionadas com as demais variáveis do modelo (Preços
Livres, Preços Administrados, Câmbio). O teste será:

\[
\begin{aligned}
& H_{0}: \text{Juros Reais não Granger-causa Preços Livres, Preços Administrados e Câmbio} \\
& H_{1}: \text{Juros Reais Granger-causa Preços Livres, Preços Administrados e Câmbio}
\end{aligned}
\]

Como resultado, se os Juros Reais não Granger-causa as demais variáveis,
o recomendado é retirar as defasagens dos Juros Reais das equações das
outras variáveis, pois seus valores passados (\(JurosReais_{t-1}\), por
exemplo) não estão contribuindo para previsão das demais variáveis.

\begin{verbatim}
## 
##  Granger causality H0: Livre do not Granger-cause Administrados
##  VarCambio VarJurosReais
## 
## data:  VAR object modelo
## F-Test = 15.8, df1 = 3, df2 = 868, p-value = 5.197e-10
\end{verbatim}

\begin{verbatim}
## 
##  Granger causality H0: Administrados do not Granger-cause Livre
##  VarCambio VarJurosReais
## 
## data:  VAR object modelo
## F-Test = 4.4907, df1 = 3, df2 = 868, p-value = 0.00389
\end{verbatim}

\begin{verbatim}
## 
##  Granger causality H0: VarCambio do not Granger-cause Livre
##  Administrados VarJurosReais
## 
## data:  VAR object modelo
## F-Test = 3.3564, df1 = 3, df2 = 868, p-value = 0.01842
\end{verbatim}

\begin{verbatim}
## 
##  Granger causality H0: VarJurosReais do not Granger-cause Livre
##  Administrados VarCambio
## 
## data:  VAR object modelo
## F-Test = 5.3901, df1 = 3, df2 = 868, p-value = 0.001118
\end{verbatim}

Uma vez que os resultados do teste de causalidade de Granger não permite
eliminar qualquer variável, vamos agora restrigir o modelo deixando
apenas variáveis que são estatisticamente significantes ao nível de 5\%
de significância. Como resultado, temos a tabela abaixo.

\begin{table}[H]
\begin{center}
\begin{tabular}{l c c c c }
\hline
 & Equação 1 & Equação 2 & Equação 3 & Equação 4 \\
\hline
Livre.l1         & $0.67 \; (0.05)^{***}$ &                        &                        & $0.08 \; (0.01)^{***}$  \\
Administrados.l1 & $0.06 \; (0.02)^{*}$   & $0.36 \; (0.06)^{***}$ &                        & $0.01 \; (0.01)^{*}$    \\
VarJurosReais.l1 & $-0.72 \; (0.26)^{**}$ & $1.62 \; (0.71)^{*}$   &                        &                         \\
const            & $0.11 \; (0.03)^{***}$ & $0.38 \; (0.06)^{***}$ &                        & $-0.05 \; (0.01)^{***}$ \\
criseeleicao     & $0.77 \; (0.14)^{***}$ & $1.05 \; (0.42)^{*}$   &                        &                         \\
VarCambio.l1     &                        & $2.76 \; (1.27)^{*}$   & $0.40 \; (0.06)^{***}$ &                         \\
\hline
R$^2$            & 0.85                   & 0.51                   & 0.16                   & 0.24                    \\
Adj. R$^2$       & 0.85                   & 0.50                   & 0.15                   & 0.23                    \\
Num. obs.        & 224                    & 224                    & 224                    & 224                     \\
RMSE             & 0.24                   & 0.70                   & 0.04                   & 0.06                    \\
\hline
\multicolumn{5}{l}{\scriptsize{$^{***}p<0.001$, $^{**}p<0.01$, $^*p<0.05$}}
\end{tabular}
\caption{Modelo VAR(1) BACEN Restrito}
\label{table:coefficients}
\end{center}
\end{table}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{7}
\tightlist
\item
  A partir da estimação do modelo, seguimos naturalmente aos testes de
  diagnóstico, a fim de verificar se os resíduos satisfazem as hipóteses
  feitas pelo método de estimação. Nesse sentido, vamos avaliar a
  presença de correlação residual bem como a estabilidade dos parâmetros
  (condição necessária mostrada anteriormente) além da correlação
  cruzada entre os resíduos. Caso as hipóteses não sejam atendidas, é
  necessário retornar ao passo 3 ou 4.
\end{enumerate}

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-8-1.pdf}

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-9-1.pdf}

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-10-1.pdf}

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-11-1.pdf}

Como é possível observar, para ambos os termos de erro de cada variável
não temos resíduos com autocorrelação (a partir da não significância
estatística das defasagens das funções FAC e FACP). Já a correlação
cruzada entre os resíduos das quatro equações pode ser verificada pela
Função de Correlação Cruzada (FCC). O gráfico mostra que nosso modelo
controlou a relação dinâmica (defasagens de ambas as séries impactando
umas às outras além delas mesmas) entre as quatro séries. O ideal é não
ter diversas defasagens estatísticamente significantes, mas apenas para
\(l=0\) ou defasagens pequenas.

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-12-1.pdf}

Para verificar a estabildiade do modelo estimado podemos obter os
autovalores associados e esperamos que eles sejam inferiores a 1 em
valor absoluto, conforme derivação das propriedades de um modelo VAR(p)
estudadas anteriormente.

\begin{verbatim}
## [1] 0.61406089 0.39960140 0.33981332 0.07617971
\end{verbatim}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{8}
\tightlist
\item
  Uma análise útil para avaliar as interações das variáveis presentes em
  um modelo VAR é a função impulso-resposta. A interpretação desta
  análise se inicia assumindo um choque de uma unidade em uma variável
  com a respectiva resposta em outra variável. Abaixo, obtemos tal
  análise como segue:
\end{enumerate}

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-14-1.pdf}

Os resultados são condizentes com o que percebemos na economia
brasileira que adotou metas de inflação. Um choque de uma unidade nos
preços livres (oscilações de mercado em função de oferta e demanda) e no
Câmbio (aqui, medido pela variação entre dois períodos consecutivos)
geram uma \textbf{pertubação rápida e crescente nos juros reais}
(medidos também pela variação entre dois períodos consecutivos).

Já um choque de uma unidade nos preços administrados (controlados por
contrato) tem um efeito crescente nos juros reais, mas bem menor em
magnitude do que o impacto causado pelas demais variáveis do modelo.

Outra análise que os modelos VAR proporcionam é avaliar a importância
das variáveis para explicar a variância do erro de previsão de cada
variável.

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-15-1.pdf}

Na prática, essa análise nos ajuda a verificar quais variáveis são
realmente importantes quando o objetivo é realizar previsões. Quanto
maior for a contribuição percentual de uma variável para a variação
total de outra, mais importante ela é para realizarmos boas previsões da
variável da qual fazemos a decomposição. Observe que a sequência
apresentada pelo R é a mesma das variáveis no nosso cojunto de dados, ou
seja, Preços Livres, Administrados, VarCambio e VarJurosReais.

Assim, na decomposição da variância do nosso modelo, verificamos que
apenas a variável DifJurosReais é explicada, em alguma relevância, pelas
demais variáveis sendo os termos de erro das variáveis preços livres e
preços administrados importantes para explicar os desvios da previsão de
VarJurosReias em relação aos valores observados.

Por fim, fazemos a previsão para a variável DifJurosReais (maior
objetivo de nossa análise). O gráfico abaixo mostra a previsão para 4
passos á frente, ou seja, para os próximos 4 meses. Praticamente, nosso
modelo está dizendo que a variação prevista para a taxa de juros reais
(SELIC menos IGPDI) é negativa e bem pequena, mantendo os juros quase
inalterados.

\includegraphics{var_bacen_files/figure-latex/unnamed-chunk-16-1.pdf}


\end{document}
